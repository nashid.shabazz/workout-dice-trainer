import React, { Component } from 'react';
import { ActivityIndicator, StyleSheet, View } from 'react-native';

import OSPicker from './utils/OSHelper.js';

class LoadScreen extends React.Component {
  static navigationOptions = {
    title: 'BazzTech™️ - Dice-Fit Trainer'
  };

  constructor(props) {
    super(props);
    this.state = {
      showComponent: true,
      isLoading: false,
    };
  }

  componentDidMount() {
    this.processLoading();
  }

  processLoading() {
    const { state, navigate } = this.props.navigation;
    let loadingTimeInMs = state.params.loadms ? state.params.loadms : 1000;
    setTimeout(() => {
      this.loadScreen(state, navigate);
      this.hideView();
    }, loadingTimeInMs);
  }

  loadScreen(state, navigate) {
    navigate(state.params.screen, {screen: state.params.screen, user: state.params.user, toast: state.params.toast})
  }

  hideView() {
    this.setState({ showComponent: false });
  }

  render() {
    if (!this.state.showComponent) {
      return null;
    } else {
      return (
        <View style={styles.container}>
          <ActivityIndicator size="large" color={styles.indicator.color} />
        </View>
      );
    }
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  indicator: {
    color: '#00ff00'
  }
});

export default LoadScreen;
