import React, { Component } from 'react';
import { Dimensions, Keyboard, Picker, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import DatePicker from 'react-native-datepicker';
import Toast from 'react-native-smart-toast';

import OSPicker from './utils/OSHelper.js';
import GoalSettings from './utils/GoalHelper.js'
import UserSettings from './utils/UserHelper.js';
import RouteButtons from './utils/RouteHelper.js';
import GlobalSettings from './utils/SettingsHelper.js';
import WorkoutSettings from './utils/WorkoutHelper.js';

class EntryInput extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      Profile: 'fit',
      Type: 'duration',
      Dice: 'home',
      Target: 'weeks',
      TargetValue: '',
      Difficulty: 'beginner',
      isInitialized: false,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.state.Target !== nextProps.state.Target && ['weeks','weight'].indexOf(nextProps.state.Target) > -1) {
      this.setState({
        TargetValue: parseInt(nextProps.state.TargetValue).toString()
      }, () => {
        this.formatNumber('TargetValue', nextProps.state, nextProps.ufunc);
      });
    }
  }

  simulateDatePress() {
    this.datePicker.onPressDate();
  }

  formatCustom(id, type, parentState, updateFunc) {
    switch (parentState.Target) {
      case 'months':
      case 'weeks':
        return isNaN(parseInt(parentState[id])) ? '1' : parseInt(parentState[id]).toString();
      case 'weight':
        return OSPicker.formatWeight(parentState[id], parentState.isMetric);
      default:
        return parentState[id];
    }
  }

  formatNumber(id, parentState, updateFunc) {
    if (id === 'TargetValue') {
      if (parentState.Target === 'weight') {
        const formattedWeight = OSPicker.formatWeight(this.state[id], parentState.isMetric, true);
        updateFunc(formattedWeight, id);
        this.setState({ TargetValue: formattedWeight });
        this.onChangeNumber(id, formattedWeight, parentState, 'numeric', updateFunc);
      } else {
        const intNumber = parseInt(this.state[id]);
        const formattedNumber = (isNaN(intNumber) ? 0 : intNumber).toString();
        updateFunc(formattedNumber, id);
        this.setState({ TargetValue: formattedNumber });
        this.onChangeNumber(id, formattedNumber, parentState, 'numeric', updateFunc);
      }
    }
  }

  formatValids(type, valids) {
    switch (type) {
      case 'bodybuild':
        delete valids.TargetValue;
      case 'duration':
        delete valids.Dice;
        break;
      case 'custom':
      default:
        break;
    }
    return valids;
  }

  onChangeForm(type, id, text, parentState, toggleButton, updateForm) {
    this.setState({ [id]: text });
    switch(type) {
      case 'picker':
        this.onChangePicker(id, text, parentState, updateForm);
        break;
      case 'numeric':
        this.onChangeNumber(id, text, parentState, type, updateForm);
        break;
      case 'text':
      default:
        this.onChangeText(id, text, type, updateForm);
        break;
    }
    parentState.isDisabled = !OSPicker.validateForm(this.formatValids(parentState.Type, Object.assign({}, parentState.valids)));
    toggleButton();
  }

  onChangePicker(id, text, parentState, updateFunc) {
    updateFunc(text, id);
    if (id === 'Target' && text === 'rhr') {
      updateFunc(true, 'valids', 'TargetValue');
    }
  }

  onChangeBirthdate(id, date, updateFunc) {
     this.setState({ Birthdate: date.toString() });
     updateFunc(OSPicker.mmddyyyyFormatDate(date.toString()), id);
     updateFunc(OSPicker.validateDate(new Date(date.toString())), 'valids', id);
  }

  onChangeNumber(id, number, parentState, type, updateFunc) {
    updateFunc((id === 'TargetValue' && parentState.Target === 'weight' && parseInt(number) === 0) ? true : OSPicker.validateNumber(number, type), 'valids', id);
  }

  onChangeText(id, text, type, updateFunc) {
    updateFunc(text, id);
    updateFunc(OSPicker.validateText(text, id), 'valids', id);
  }

  onFocus(id, updateFunc) {
    updateFunc(id, 'activeView');
  }

  toggleType(type, id, choices, parentState, updateFunc) {
    if (type === 'picker') {
      const nextValue = OSPicker.nextQueueValue(Object.values(choices).map(c => OSPicker.trim(c, true).toLowerCase()), OSPicker.trim(parentState[id] === 'rhr' ? 'heartrate' : parentState[id], true));
      updateFunc(nextValue, id);
      if (nextValue === 'heartrate') {
        this.onChangePicker('Target', 'rhr', parentState, updateFunc);
      }
      parentState.isDisabled = !OSPicker.validateForm(this.formatValids(parentState.Type, Object.assign({}, parentState.valids)));
    }
  }

  render() {
    const validationTextColor = this.props.state.valids[this.props.id] ? 'green' : 'red';
    const validationTitleColor = this.props.state.valids[this.props.id] ? 'black' : 'red';
    let entryInput;
    switch (this.props.type) {
      case 'date':
        entryInput = (
          <View>
            <DatePicker
              ref={(picker) => { this.datePicker = picker; }}
              style={OSPicker.isPlatformIOS() ? {width: 0, height: 0} : {width: 275, display: 'none'}}
              date={this.state[this.props.id]}
              mode="date"
              placeholder="select date"
              format="MM/DD/YYYY"
              minDate="01/01/1900"
              maxDate={OSPicker.mmddyyyyFormatDateYYIncr((new Date()).toString(), -5)}
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              customStyles={{
                dateIcon: {
                  position: 'absolute',
                  left: 0,
                  top: 4,
                  marginLeft: 0
                },
                dateInput: {
                  marginLeft: 36
                }
              }}
              onDateChange={(date) => { this.onChangeForm(this.props.type, this.props.id, date, this.props.state, this.props.func, this.props.ufunc); }}
            />
            <TouchableOpacity onPress={() => { this.simulateDatePress(); }}>
              <View pointerEvents='none'>
                <TextInput
                  style={{fontSize: 20, width: 275, height: 45, textAlign: 'center', color: validationTextColor }}
                  keyboardType={this.props.keyBoardType}
                  value={OSPicker.mmddyyyyFormatDate(this.state[this.props.id])}
                />
              </View>
            </TouchableOpacity>
          </View>
        );
        break;
      case 'picker':
        const keys = Object.keys(this.props.choices);
        let items = [];
        for (let k in keys) {
          const c = keys[k];
          items = items.concat([ <Picker.Item key={this.props.choices[c]} label={this.props.choices[c]} color={validationTextColor} value={c} /> ]);
        }
        entryInput = (
          <View ref={(pickerView) => { this.pickerView = pickerView; }} style={{paddingBottom: OSPicker.isPlatformDroid ? 10 : 0}}>
            <View style={OSPicker.isPlatformDroid ? {borderBottomColor: 'black', borderBottomWidth: 1} : {}}>
              <Picker
                style={OSPicker.isPlatformDroid ? {alignItems: 'center'} : {}, {width: 175}}
                itemStyle={OSPicker.isPlatformIOS ? {color: validationTextColor, height: 90} : {}}
                selectedValue={this.props.state[this.props.id]}
                onValueChange={(val) => { this.onChangeForm(this.props.type, this.props.id, val, this.props.state, this.props.func, this.props.ufunc); }} >
                {items}
              </Picker>
            </View>
          </View>
        );
        break;
      case 'numeric':
        entryInput = (
          <TextInput
            style={{fontSize: 20, width: 225, height: 45, textAlign: 'center', color: validationTextColor }}
            keyboardType={this.props.type}
            selectTextOnFocus={true}
            maxLength={this.props.id === 'TargetValue' && this.props.state.Target === 'weight' ? 8 : 3}
            value={this.state[this.props.id].toString()}
            onFocus={() => { this.onFocus(this.props.id, this.props.ufunc); }}
            onEndEditing={() => { this.formatNumber(this.props.id, this.props.state, this.props.ufunc); }}
            onChangeText={(text) => { this.onChangeForm(this.props.type, this.props.id, text, this.props.state, this.props.func, this.props.ufunc); }}
          />
        );
        break;
      default:
        entryInput = (
          <TextInput
            style={{fontSize: 20, width: 275, height: 45, textAlign: 'center', color: validationTextColor }}
            autoCapitalize={this.props.autoCapitalize ? this.props.autoCapitalize : 'none'}
            keyboardType={this.props.type}
            maxLength={100}
            placeholder={this.props.placeholder}
            value={this.props.state[this.props.id]}
            onFocus={() => { this.onFocus(this.props.id, this.props.ufunc); }}
            onChangeText={(text) => { this.onChangeForm(this.props.type, this.props.id, text, this.props.state, this.props.func, this.props.ufunc); }}
          />
        );
        break;
    }
    return (
      <View style={(this.props.state.keyboardSpace && this.props.id !== this.props.state.activeView) ? {display: 'none', marginBottom: 0} : ((this.props.state.keyboardSpace && this.props.id === this.props.state.activeView) ? Object.assign({position: 'absolute'}, this.props.state.deviceOrientation === 'portrait' ? {bottom: this.props.state.keyboardSpace} : {top: 10}) : {marginBottom: 10})}>
        {entryInput}
        <TouchableOpacity onPress={() => { this.toggleType(this.props.type, this.props.id, this.props.choices, this.props.state, this.props.ufunc); }}>
          <Text style={{ fontSize: 15, textAlign: 'center', color: validationTitleColor, lineHeight: 15, transform: [{scaleX: 1.5}] }}>
            {OSPicker.splitProperNouns(this.props.id)}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

class Portal extends React.Component {

  componentWillReceiveProps(nextProps) {
    if (nextProps.state.deviceOrientation != this.props.state.deviceOrientation ||
      nextProps.state.Type != this.props.state.Type && nextProps.state.Type === 'custom') {
      nextProps.onupdate(true, 'isScroll');
    }
  }

  onScroll = (e, parentState, updateFunc) => {
    if (parentState.isScroll === true) {
      const height = e.nativeEvent.contentSize.height + parentState.footerHeight,
          offset = e.nativeEvent.contentOffset.y;
      if (parseInt(offset + height) >= parseInt(parentState.screenHeight)) {
          updateFunc(false, 'isScroll');
      }
    }
  }

  render() {
    const profileChoices = this.props.state.userAge < 18 ? {fit: 'Fit', muscular: 'Muscular'} : {fit: 'Fit', slim: 'Slim', muscular: 'Muscular'};
    let entries = [
      <EntryInput key={0} id='Profile' type={'picker'} state={this.props.state} choices={profileChoices} func={this.props.onchange} ufunc={this.props.onupdate} />,
      <EntryInput key={1} id='Type' type={'picker'} state={this.props.state} choices={{duration: 'Duration', bodybuild: 'Body Build', custom: 'Custom'}} func={this.props.onchange} ufunc={this.props.onupdate} />,
    ];
    switch (this.props.state.Type) {
      case 'bodybuild':
        entries = entries.concat([<EntryInput key={2} id='Target' type={'picker'} state={this.props.state} choices={{average: 'Average', toned: 'Toned', thin: 'Thin'}} func={this.props.onchange} ufunc={this.props.onupdate} />]);
        break;
      case 'custom':
        entries = entries.concat([
          <EntryInput key={3} id='Dice' type={'picker'} state={this.props.state} choices={{home: 'Home', gym: 'Gym', combo: 'Combo'}} func={this.props.onchange} ufunc={this.props.onupdate} />,
          <EntryInput key={2} id='Target' type={'picker'} state={this.props.state} choices={{rhr: 'Heart Rate', weight: 'Weight', weeks: 'Weeks', months: 'Months'}} func={this.props.onchange} ufunc={this.props.onupdate} />
        ]);
        if (this.props.state.Target === 'rhr') {
          entries = entries.concat([<EntryInput key={4} id='TargetValue' type={'picker'} state={this.props.state} choices={{average: 'Average', aboveavg: 'Above Avg', good: 'Good', excellent: 'Excellent', athlete: 'Athlete'}} func={this.props.onchange} ufunc={this.props.onupdate} />]);
        } else {
          entries = entries.concat([<EntryInput key={4} id='TargetValue' type={'numeric'} state={this.props.state} func={this.props.onchange} ufunc={this.props.onupdate} />]);
        }
        break;
      default:
      case 'duration':
        entries = entries.concat([
          <EntryInput key={2} id='Target' type={'picker'} state={this.props.state} choices={{weeks: 'Weeks', months: 'Months'}} func={this.props.onchange} ufunc={this.props.onupdate} />,
          <EntryInput key={4} id='TargetValue' type={'numeric'} state={this.props.state} func={this.props.onchange} ufunc={this.props.onupdate} />
        ]);
        break;
    }

    entries = entries.concat([<EntryInput key={5} id='Difficulty' type={'picker'} state={this.props.state} choices={{beginner: 'Beginner', trainee: 'Trainee', expert: 'Expert'}} func={this.props.onchange} ufunc={this.props.onupdate} />]);
    if (entries.length) {
      return (
        <ScrollView ref={(scrollView) => { this.scrollView = scrollView; }} contentContainerStyle={styles.container} onScroll={(event) => { this.onScroll(event, this.props.state, this.props.onupdate); }}>
          {entries}
        </ScrollView>
      );
    }
    return ( <ScrollView contentContainerStyle={styles.container}><Text>{'Content failed to load'}</Text></ScrollView> );
  }
}

class ProfileScreen extends React.Component {
  static navigationOptions = {
      title: 'BazzTech™️ - Dice-Fit Trainer'
  };

  constructor(props) {
      super(props);
      this.state = {
        valids: { Dice: true, Difficulty: true, Profile: true, Target: true, TargetValue: false, Type: true },
        showComponent: true,
        isDisabled: true,
        isMetric: false,
        isScroll: false,
        Profile: 'fit',
        Type: 'duration',
        Dice: 'home',
        Target: 'weeks',
        TargetValue: '0',
        userAge: 0,
        currentWorkoutId: -1,
        activeView: '',
        keyboardSpace: 0,
        screenWidth: 0,
        screenHeight: 0,
        footerHeight: 30,
        deviceOrientation: OSPicker.getDeviceOrientation(),
        Difficulty: 'beginner'
      };
      this.indicator = null;
      this.toggleRouteButton = this.toggleRouteButton.bind(this);
      this.updateFormFields = this.updateFormFields.bind(this);
      Dimensions.addEventListener('change', () => {
        this.setState({
          screenWidth: Dimensions.get('window').width,
          screenHeight: Dimensions.get('window').height,
          deviceOrientation: OSPicker.getDeviceOrientation()
        }, () => {
          this.clearIndicator();
          if (this.state.keyboardSpace) {
            Keyboard.dismiss();
          }
        });
      });
      Keyboard.addListener('keyboardDidShow', (frames) => {
        if (!frames.endCoordinates) return;
        this.setState({keyboardSpace: frames.endCoordinates.height});
      });
      Keyboard.addListener('keyboardDidHide', (frames) => {
        this.setState({keyboardSpace: 0, activeView: '' });
      });
  }

  componentDidMount() {
    this.onLayout();
    const params = this.props.navigation.state.params;
    GlobalSettings.isMetricSystem(params && params.user && params.user.settings ? params.user.settings.units : null).then(isMetricSystem => {
      this.setState({
        isMetric: isMetricSystem,
      });
      UserSettings.getUser((params && params.user) ? params.user : null).then(user => {
        this.setState({
          userAge: UserSettings.getUserAge(new Date(user.dob)),
          currentWorkoutId: user.workoutId
        });
      });
    });
  }

  componentWillMount() {
    this.setState({
      screenWidth: Dimensions.get('window').width,
      screenHeight: Dimensions.get('window').height,
      deviceOrientation: OSPicker.getDeviceOrientation()
    });
  }

  componentWillUnmount() {
    this.clearIndicator();
  }

  clearIndicator() {
    if (this.indicator != null) {
      this.hideToast();
      clearInterval(this.indicator);
    }
  }

  toggleRouteButton() {
    this.setState(state => ({
      disabled: state.isDisabled
    }));
  }

  updateFormFields(value, id1, id2 = null) {
    let obj = this.state[id1];
    if (id2 === null) {
      obj = value;
      this.setState({
        [id1]: value
      });
      // Handle custom validation
      let vobj = this.state.valids;
      if (id1 === 'Target') {
        vobj.TargetValue = (value === 'weight') ? true : this.state.TargetValue !== '0';
      }
      this.setState({
        valids: vobj
      });
    } else {
      obj[id2] = value;
      this.setState({
        [id1]: obj
      });
    }
  }

  showToast() {
    if (this.toast) {
      this.toast.show({
        duration: 1000,
        position: Toast.constants.gravity.bottom,
        children: <View><Text style={{color: '#008000'}}>{'▼'}</Text></View>,
        animationEnd: () => { this.hideToast(); }
      });
    }
  }

  hideToast() {
    if (this.toast) {
      this.toast.hide({ duration: 400 });
    }
  }

  onLayout() {
    this.showToast();
    this.indicator = setInterval(() => {
      if (this.state.isScroll) {
        this.showToast();
      } else {
        this.clearIndicator();
      }
    }, 2000);
  }

  beginWorkout(goToScreen) {
    const workout = {
      profile: this.state.Profile,
      type: this.state.Type,
      dice: this.state.Dice,
      target: this.state.Target,
      targetValue: this.state.TargetValue,
      difficulty: this.state.Difficulty
    };

    const updateGoalPromise = new Promise((resolve, reject) => {
      GoalSettings.updateGoal(this.state.currentWorkoutId, { active: false }).then((goals) => {
        resolve(goals);
      }).catch(error => {
        console.error(error.message);
        resolve(null);
      });
    });
    WorkoutSettings.addWorkout(workout).then(newWorkout => {
      updateGoalPromise.then(() => {
        GoalSettings.addGoal({
          id: newWorkout.id,
          active: true,
          workout: {
            options: {
              profile: newWorkout.profile,
              type: newWorkout.type,
              dice: newWorkout.type !== 'custom' ? null : newWorkout.dice,
              target: newWorkout.target,
              targetValue: newWorkout.type === 'bodybuild' ? null : ((newWorkout.type === 'custom' && newWorkout.target === 'weight') ? parseInt(newWorkout.targetValue) : newWorkout.targetValue),
              difficulty: newWorkout.difficulty
            }
          }
        }).then(() => {
          let userUpdate = {workoutId: newWorkout.id, progress: 0};
          if (newWorkout.type === 'custom' && ['rhr', 'weight'].indexOf(newWorkout.target) > -1) {
            userUpdate = Object.assign(userUpdate, { [newWorkout.target]: newWorkout.target === 'rhr' ? '120' : OSPicker.convertWeight('999 lbs', this.state.isMetric) });
          }
          UserSettings.updateUser(userUpdate)
          .then(data => {
            this.hideView();
            goToScreen('LoadScreen', {screen: 'WorkoutScreen', user: data});
          }).catch(error => {
            console.error(error.message);
          });
        });
      });
    })
  }

  hideView() {
    this.setState({ showComponent: false });
  }

  render() {
    const { state, navigate } = this.props.navigation;
    const isPortraitMode = this.state.deviceOrientation === 'portrait';
    const newWorkoutTitle = isPortraitMode ? 'Begin Workout' : 'Begin New Workout';
    const rbuttons = [
        { title: newWorkoutTitle, func: () => this.beginWorkout(navigate), disabled: this.state.isDisabled }
    ];
    if (!this.state.showComponent) {
      return null;
    } else {
      return (
        <View style={{flex: 1, flexDirection: 'column', justifyContent: 'space-between'}} onLayout={() => { this.onLayout(); }}>
          <Portal state={this.state} onchange={this.toggleRouteButton} onupdate={this.updateFormFields} />
          <Toast ref={(toast) => { this.toast = toast; }} spacing={100} leftX={this.state.screenWidth * 0.95} style={{backgroundColor: 'transparent'}}></Toast>
          <RouteButtons buttons={rbuttons} />
        </View>
      );
    }
  }
};

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    flexDirection: 'column',
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  }
});

export default ProfileScreen;
