import React, { Component } from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';

import OSPicker from '../utils/OSHelper.js';

class CheckBox extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    let mediaKey = (this.props.disabled ? 'no-' : '') + 'checkbox-' + (this.props.checked ? 'on' : 'off');
    return (
      <TouchableOpacity
        checked={this.props.checked}
        disabled={this.props.disabled}
        onPress={() => { this.props.func(); }}>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <Image
            style={this.props.style}
            source={{ uri: OSPicker.getImgData().media[(this.props.disabled ? 'no-' : '') + 'checkbox-' + (this.props.checked ? 'on' : 'off') + '-alt'] }}
          />
          <Text>{this.props.title}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

export default CheckBox;
