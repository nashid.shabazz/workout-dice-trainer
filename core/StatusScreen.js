import React, { Component } from 'react';
import { Animated, Dimensions, Image, ScrollView, StyleSheet, Text, TextInput, TouchableHighlight, TouchableOpacity, View } from 'react-native';
import TimerEnhance from 'react-native-smart-timer-enhance';
import FloatingHearts from 'react-native-floating-hearts';
import { LinearGradient } from 'expo';

import OSPicker from './utils/OSHelper.js';
import UserSettings from './utils/UserHelper.js';
import GoalSettings from './utils/GoalHelper.js';
import RouteButtons from './utils/RouteHelper.js';
import GlobalSettings from './utils/SettingsHelper.js';
import WorkoutSettings from './utils/WorkoutHelper.js';

class EntryProfile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 0,
      counter: 50,
      PI: 0,
      BMI: 0,
      MHR: 0,
      RHR: '',
      ERHR: 0,
      Weight: '',
      RHRPrevious: '',
      WeightPrevious: '',
      isInitialized: false,
      intervalId: null,
    };
  }

  componentDidMount() {
    setTimeout(() => {
      this.setState({
        Weight: this.props.state.Weight,
        RHR: this.props.state.RHR
      });
    }, 250);
  }

  componentWillUnmount() {
    if (this.state.intervalId != null) {
      clearInterval(this.state.intervalId);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (!this.state.isInitialized) {
      this.setState({
        RHR: nextProps.state.RHR,
        Weight: nextProps.state.Weight,
        isInitialized: true
      });
    }
  }

  formatCustom(id, text, parentState, updateFunc) {
    let formattedValue;
    switch (id) {
      case 'BodyBuild':
        const wnum = parseInt(parentState['Weight']);
        const weight = isNaN(wnum) ? 0 : wnum;
        const hstr = parentState['Height'] ? parentState['Height'].trim().replace(/['"]+/g, '').substring(0, 3) : '0';
        const height = parentState['isMetric'] ? parseInt(hstr) : parseInt(hstr[0]) * 12 + parseInt(hstr.substring(1, 3));
        return OSPicker.getBodyBuild(parseFloat(OSPicker.getPI(height, weight, parentState['isMetric'], false)), parentState['Gender'], parentState['isMetric']);
      case 'Weight':
        formattedValue = OSPicker.formatWeight(text, parentState.isMetric);
        break;
      case 'RHR':
        formattedValue = OSPicker.formatRHR(text);
        break;
      default:
        formattedValue = text;
        break;
    }
    updateFunc(formattedValue, id);
    return formattedValue;
  }

  formatNumber(id, parentState, updateFunc) {
    switch (id) {
      case 'PI':
      case 'BMI':
        const wnum = parseInt(parentState['Weight']);
        const weight = isNaN(wnum) ? 0 : wnum;
        const hstr = parentState['Height'] ? parentState['Height'].trim().replace(/['"]+/g, '').substring(0, 3) : '0';
        const height = parentState['isMetric'] ? parseInt(hstr) : parseInt(hstr[0]) * 12 + parseInt(hstr.substring(1, 3));
        return id === 'PI' ? OSPicker.getPI(height, weight, parentState['isMetric'], false) : OSPicker.getBMI(height, weight, parentState['isMetric'], false);
      case 'ERHR':
        return OSPicker.getEHRStatus(parentState['Age'], parentState['RHR']);
      case 'MHR':
        return 220 - parentState['Age'];
      default:
        break;
    }
  }

  setImageSource(src, parentState, updateFunc) {
    this.setState({
      imageSource: src,
    });

    if (this.state.intervalId == null && parentState.Progress >= 1) {
      const intervalId = setInterval(() => {
        this.touchable.props.onPress();
      }, 900);
      this.setState({intervalId: intervalId});
      updateFunc(intervalId, 'animationIntervalId');
    }
  }

  renderUnits = (type, parentState) => {
    const units = OSPicker.getUnits(type, parentState['isMetric']).split('/');
    const getSingleUnit = function(t) { return <Text adjustsFontSizeToFit={true} style={{color: '#008000', textAlign: 'center', textAlignVertical: 'center'}}>{t}</Text>; };

    return (
      <View style={{flexDirection: 'column', marginLeft: 5, justifyContent: 'center'}}>
        {getSingleUnit(units[0])}
        <View
          style={{
            height: 1,
            width: 25,
            marginTop: 0,
            marginBottom: 0,
            backgroundColor: '#008000',
          }}
        />
        {getSingleUnit(units[1])}
      </View>
    );
  }

  showHelpDialog(type) {
    let message;
    switch (type.toLowerCase()) {
      case 'bodybuild':
        message = '\"Body Build\" is an estimated measure of body physique based on Ponderal Index and empirical data';
        break;
      case 'bmi':
        message = '\"Body Mass Index\" (' + type.toUpperCase() + ') is a measure of body fat based on height and weight';
        break;
      case 'pi':
        message = '\"Ponderal Index\" (' + type.toUpperCase() + ') is a measure of leanness based on height and weight';
        break;
      case 'rhr':
        message = '\"Resting Heart Rate\" (' + type.toUpperCase() + ') is a measure of heart beats per minute at complete rest';
        break;
      case 'erhr':
        message = '\"Estimated Resting Heart Rate\" (' + type.toUpperCase() + ') is an estimated measure of heart beats per minute at complete rest based on age';
        break;
      case 'mhr':
        message = '\"Maximum Heart Rate\" (' + type.toUpperCase() + ') is an estimated measure of heart beats per minute at maximum effort based on age';
        break;
      case 'weight':
      default:
        message = '\"Weight\" is a body\'s mass in relation to gravity which can be used to assess physical fitness';
        break;
    }
    OSPicker.showHelpDialog(message);
  }

  updateUserProgress(id, parentState, updateFunc) {
    UserSettings.updateUser({ [id.toLowerCase()]: this.state[this.props.id] }).then((user) => {
      UserSettings.updateProgress().then((progress) => {
        if (progress >= 1.0) {
          const msg = 'Submitting this ' + this.props.id + ' value will complete the workout!';
          OSPicker.showConfirmDialog('Complete Workout?', msg, null, true, () => {
            updateFunc(progress, 'Progress');
            const tdate = (new Date()).toString();
            GoalSettings.updateGoal(parentState.currentWorkoutId, { edate: tdate, progress: progress })
            .catch(error => {
              console.error(error.message);
            }).finally(() => {
              OSPicker.playStatusComplete(true);
            });
          }, () => {
            let previousFormattedValue;
            switch (id) {
              case 'Weight':
                previousFormattedValue = OSPicker.formatWeight(this.state.WeightPrevious, parentState.isMetric);
                break;
              case 'RHR':
                previousFormattedValue = OSPicker.formatRHR(this.state.RHRPrevious);
                break;
            }
            this.setState({ [id]: previousFormattedValue });
            updateFunc(previousFormattedValue, id);
          });
        } else {
          updateFunc(progress, 'Progress');
          GoalSettings.updateGoal(parentState.currentWorkoutId, { progress: progress })
          .catch(error => {
            console.error(error.message);
          });
        }
      });
    })
  }

  showDialog(id, description) {
    OSPicker.showInfoDialog(id, description);
  }

  render() {
    const validationTextColor = 'green';
    const validationTitleColor = 'black';
    const isPortraitMode = this.props.state.deviceOrientation === 'portrait';
    let entryProfile;
    switch (this.props.type) {
      case 'text':
        const profileDescription = OSPicker.isEmpty(this.props.state.Profile) ? 'None' : OSPicker.wordToProperCase(this.props.state.Profile) + ' (' + OSPicker.wordToProperCase(this.props.state.Difficulty) + ')';
        entryProfile = (
          <TouchableOpacity style={{flexDirection: 'row', alignSelf: 'flex-end'}} onLongPress={() => { this.showDialog(this.props.id, profileDescription); }}>
              <Text adjustsFontSizeToFit={true} numberOfLines={1} style={{fontSize: 20, width: 200, height: 45, marginTop: 10}}>
              {this.props.id + ': '}<Text style={{color: '#008000'}}>{profileDescription}</Text>
              </Text>
          </TouchableOpacity>
        );
        break;
      case 'picture':
        const a = function(i, c, len, p) {
          const idx = len - i - 1;
          const med = Math.round((len - 1) * p);
          const cvg = 0.1;
          return c.substring(0, c.lastIndexOf(',') + 1) + ((p == 1) ? 1 :((idx === med) ? (cvg) : (idx < med ? 1 : 0))) + ')';
        };
        const { count } = this.state;
        const colors = ['rgba(139,0,0,1)', 'rgba(255,0,0,1)', 'rgba(126,83,0,1)', 'rgba(255,165,0,1)', 'rgba(255,255,0,1)', 'rgba(0,255,0,1)', 'rgba(0,255,255,0.1)', 'rgba(0,0,255,0)', 'rgba(238,130,238,0)', 'rgba(224,30,221,1)', 'rgba(111,15,110,1)', 'rgba(44,6,44,1)'];
        const gradientColors = colors.reverse().map((c,i) => a(i, c, colors.length, this.props.state.Progress));
        entryProfile = (
          <View style={{justifyContent: 'flex-start'}} onLayout={(event) => { this.setImageSource(event.nativeEvent.layout, this.props.state, this.props.ufunc); }}>
            <LinearGradient
              colors={gradientColors}
              style={{width: 100, height: 330}}>
              <TouchableHighlight onLongPress={() => { this.showHelpDialog('BodyBuild'); }}>
                <Text adjustsFontSizeToFit={true} numberOfLines={1} style={{backgroundColor: 'white', lineHeight: 15, textAlign: 'center'}}>
                  {this.formatCustom('BodyBuild', null, this.props.state, this.props.ufunc)}
                </Text>
              </TouchableHighlight>
              <Image
                style={{height: 300, resizeMode: Image.resizeMode.stretch}}
                source={{ uri: OSPicker.getImgData().media['avatar-' + this.props.state.Gender.toLowerCase()] }}
              />
              <Text adjustsFontSizeToFit={true} numberOfLines={1} style={{backgroundColor: 'white', lineHeight: 15, textAlign: 'center'}}>
                {this.props.state.Nickname}
              </Text>
              <TouchableOpacity ref={(touchable) => { this.touchable = touchable; }} onPress={() => this.setState({ count: count + 1 })} />
            </LinearGradient>
            <FloatingHearts count={count} color={'#006400'} source={require('./assets/graphics/star.png')} rightmax={300} />
          </View>
        );
        break;
      case 'numeric':
        const textInputWidth = isPortraitMode ? 125 : 200;
        entryProfile = (
          <View>
            <TextInput
              style={{fontSize: 20, width: textInputWidth, height: 45, textAlign: 'center', alignSelf: 'center', color: validationTextColor}}
              keyboardType={this.props.type}
              selectTextOnFocus={true}
              editable={this.props.state.Progress < 1.0}
              maxLength={this.props.id === 'Weight' ? 8 : 3}
              value={this.state[this.props.id]}
              onFocus={() => { this.setState({ [this.props.id + 'Previous']: this.state[this.props.id]}); }}
              onEndEditing={() => { this.updateUserProgress(this.props.id, this.props.state, this.props.ufunc); }}
              onChangeText={(text) => { this.setState({ [this.props.id]: this.formatCustom(this.props.id, text, this.props.state, this.props.ufunc) }); }}
            />
            <TouchableOpacity onLongPress={() => { this.showHelpDialog(this.props.id); }}>
              <Text style={{ fontSize: 15, textAlign: 'center', color: validationTitleColor, lineHeight: 15, transform: [{scaleX: 1.5}] }}>
                {this.props.id}
              </Text>
            </TouchableOpacity>
          </View>
        );
        break;
      default:
        const touchableFontSize = isPortraitMode ? 15 : 20;
        const touchableWidth = isPortraitMode ? 90 : 100;
        const touchableUnits = (['PI', 'BMI'].indexOf(this.props.id) > -1) ? this.renderUnits(this.props.id, this.props.state) : undefined;
        entryProfile = (
          <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end'}}>
            <TouchableOpacity style={{justifyContent: 'center'}} onLongPress={() => { this.showHelpDialog(this.props.id); }}>
              <Text adjustsFontSizeToFit={true} style={{fontSize: touchableFontSize, textAlign: 'center'}}>{this.props.id}</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{width: touchableWidth, backgroundColor: 'rgba(0,128,0,0.04)', margin: 5, justifyContent: 'center', alignItems: 'stretch'}} onLongPress={() => { this.showHelpDialog(this.props.id); }}>
              <View style={{flexDirection: 'row', height: 40, justifyContent: 'center', borderColor: 'black', borderRadius: 5, borderWidth: 2}}>
                <View style={{flexDirection: 'column', justifyContent: 'center'}}><Text adjustsFontSizeToFit={true} numberOfLines={1} style={{color: '#008000', textAlign: 'center', textAlignVertical: 'center'}}>{this.formatNumber(this.props.id, this.props.state, this.props.ufunc)}</Text></View>
                {touchableUnits}
              </View>
            </TouchableOpacity>
          </View>
        );
        break;
    }
    return (
      <View style={{marginBottom: this.props.id === 'Weight' ? 15 : 0}}>
        {entryProfile}
      </View>
    );
  }
}

class Portal extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      intervalId: null,
      showAnimation: true
    };
  }

  componentWillUnmount() {
    clearInterval(this.state.intervalId);
  }

  clearInterval() {
    if (this.state.intervalId != null) {
      clearInterval(this.state.intervalId);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.state.Progress !== this.props.state.Progress) {
      if (this.state.intervalId === null && nextProps.state.Progress >= 1.0) {
        this.startProgress(this.props.onupdate);
      } else if (this.state.intervalId !== null && nextProps.state.Progress < 1.0) {
        this.clearInterval();
      }
    }
  }

  startProgress(updateFunc) {
    if (this.props.state.Progress >= 1.0) {
      const intervalId = setInterval(() => {
        this.setState({
          showAnimation: !this.state.showAnimation
        });
      }, 1000);
      this.setState({ intervalId: intervalId });
      updateFunc(intervalId, 'progressIntervalId');
    }
  }

  render() {
    const entries = [
      <EntryProfile key={1} id='RHR' type={'numeric'} state={this.props.state} func={this.props.onchange} ufunc={this.props.onupdate} />,
      <EntryProfile key={2} id='Weight' type={'numeric'} state={this.props.state} func={this.props.onchange} ufunc={this.props.onupdate} />,
      <EntryProfile key={3} id='BMI' type={'default'} state={this.props.state} func={this.props.onchange} ufunc={this.props.onupdate} />,
      <EntryProfile key={4} id='PI' type={'default'} state={this.props.state} func={this.props.onchange} ufunc={this.props.onupdate} />,
      <EntryProfile key={5} id='ERHR' type={'default'} state={this.props.state} func={this.props.onchange} ufunc={this.props.onupdate} />,
      <EntryProfile key={6} id='MHR' type={'default'} state={this.props.state} func={this.props.onchange} ufunc={this.props.onupdate} />,
      <EntryProfile key={7} id='Profile' type={'text'} state={this.props.state} func={this.props.onchange} ufunc={this.props.onupdate} />,
    ];

    const progress = parseInt(this.props.state.Progress * 100);
    const percent = (!this.state.showAnimation) ? '' : (progress > 100 ? 100 : progress) + '%';
    if (true) {
      return (
        <ScrollView
        contentContainerStyle={styles.container}
        showsVerticalScrollIndicator={false}>
          <View style={{flex: 0.395, marginLeft: 5}} onLayout={() => { this.startProgress(this.props.onupdate); }}>
            <EntryProfile key={0} id='Avatar' type={'picture'} state={this.props.state} func={this.props.onchange} ufunc={this.props.onupdate} />
          </View>
          <Text adjustsFontSizeToFit={true} numberOfLines={1} style={{flex: 0.21, textAlign: 'center', color: (this.state.showAnimation ? '#006400' : 'black'), fontSize: progress >= 100 ? 26.5 : 30}}>{percent}</Text>
          <View style={{flex: 0.395, marginRight: 5}}>
            {entries}
          </View>
        </ScrollView>
      );
    }
    return ( <ScrollView contentContainerStyle={styles.container}><Text>{'Content failed to load'}</Text></ScrollView> );
  }
}

class StatusScreen extends React.Component {
  static navigationOptions = {
      title: 'BazzTech™️ - Dice-Fit Trainer'
  };

  constructor(props) {
      super(props);
      this.state = {
        valids: { RHR: true, Weight: true },
        showComponent: true,
        isDisabled: true,
        isMetric: false,
        Gender: 'male',
        PI: 0,
        BMI: 0,
        MHR: 0,
        RHR: '',
        ERHR: 0,
        Age: 0,
        Weight: '',
        Height: '5\'6\"',
        Progress: 0,
        Nickname: 'nickname',
        BodyBuild: '',
        Profile: '',
        Difficulty: '',
        currentWorkoutId: 0,
        screenWidth: 0,
        screenHeight: 0,
        animationIntervalId: null,
        progressIntervalId: null,
        deviceOrientation: OSPicker.getDeviceOrientation(),
      };
      this.toggleRouteButton = this.toggleRouteButton.bind(this);
      this.updateFormFields = this.updateFormFields.bind(this);
      Dimensions.addEventListener('change', () => {
        this.setState({
          screenWidth: Dimensions.get('window').width,
          screenHeight: Dimensions.get('window').height,
          deviceOrientation: OSPicker.getDeviceOrientation()
        });
      });
  }

  componentWillMount() {
    this.setState({
      screenWidth: Dimensions.get('window').width,
      screenHeight: Dimensions.get('window').height,
      deviceOrientation: OSPicker.getDeviceOrientation()
    });
  }

  componentDidMount() {
    const params = this.props.navigation.state.params;
    GlobalSettings.isMetricSystem(params && params.user && params.user.settings ? params.user.settings.units : null).then(isMetricSystem => {
      UserSettings.getUser().then(user => {
        if (!OSPicker.isUndefinedOrNull(user)) {
          WorkoutSettings.getWorkout(user.workoutId).then((workout) => {
            this.setState({
              Profile: workout.profile,
              Difficulty: workout.difficulty,
            });
          }).finally(() => {
            this.setState({
              Age: UserSettings.getUserAge(new Date(user.dob)),
              RHR: user.rhr,
              Gender: user.gender,
              isMetric: isMetricSystem,
              Height: OSPicker.convertHeight(user.height, isMetricSystem),
              Weight: OSPicker.convertWeight(user.weight, isMetricSystem),
              currentWorkoutId: user.workoutId,
              Nickname: user.nick,
              Progress: (isNaN(user.progress) || user.progress < 0) ? 0 : user.progress,
            });
            if (user.progress >= 1.0) {
              OSPicker.playStatusComplete();
            }
          });
        }
      });
    }).catch(error => {
      console.error(error.message);
    });
  }

  componentWillUnmount() {
    this.clearProgress();
  }

  toggleRouteButton() {
    this.setState(state => ({
      disabled: state.isDisabled
    }));
  }

  updateFormFields(value, id1, id2 = null) {
    let obj = this.state[id1];
    if (id2 === null) {
      obj = value;
      this.setState({
        [id1]: value
      });
      // Handle custom validation
      const vobj = this.state.valids;
      this.setState({
        valids: vobj
      });
    } else {
      obj[id2] = value;
      this.setState({
        [id1]: obj
      });
    }
  }

  startNewWorkout(goToScreen, data) {
    OSPicker.showConfirmDialog('New Workout', 'Are you sure?', null, true, () => {
      this.clearProgress();
      goToScreen('LoadScreen', {screen: 'ProfileScreen', user: data});
    });
  }

  continueCurrentWorkout(goToScreen, data) {
    if (this.state.Progress >= 1.0) {
      let msg = 'Review progress for COMPLETED workout';
      OSPicker.showConfirmDialog('Continue Workout?', msg, null, true, () => {
        this.clearProgress();
        this.hideView();
        goToScreen('LoadScreen', {screen: 'WorkoutScreen', user: data});
      });
    } else {
      this.hideView();
      goToScreen('LoadScreen', {screen: 'WorkoutScreen', user: data});
    }
  }

  hideView() {
    this.setState({ showComponent: false });
  }

  clearProgress() {
    if (this.state.animationIntervalId !== null) {
      clearInterval(this.state.animationIntervalId);
    }
    if (this.state.progressIntervalId !== null) {
      clearInterval(this.state.progressIntervalId);
    }
  }

  render() {
    const { state, navigate } = this.props.navigation;
    const isPortraitMode = this.state.deviceOrientation === 'portrait';
    let newWorkoutTitle = isPortraitMode ? 'New Workout' : 'Try New Workout';
    const workoutTitle = isPortraitMode ? 'Continue' : 'Continue Workout';
    let counterTitle = isPortraitMode ? 'Set Counter' : 'Use Set Counter';
    if ((isPortraitMode || this.state.screenWidth <= 480) && OSPicker.getDeviceAspectRatio() <= 1.5) {
      newWorkoutTitle = isPortraitMode ? 'New' : 'New Workout';
      counterTitle = isPortraitMode ? 'Counter' : 'Set Counter';
    }
    let rbuttons = [
        { title: newWorkoutTitle, func: () => this.startNewWorkout(navigate, state.params.user), disabled: false },
    ];
    if (OSPicker.isEmpty(this.state.Profile)) {
      rbuttons = rbuttons.concat([{ title: counterTitle, func: () => { this.hideView(); navigate("LoadScreen", {screen: "CounterScreen", user: state.params.user, loadms: 200}); }, disabled: false }]);
    } else {
      rbuttons = rbuttons.concat([{ title: workoutTitle, func: () => this.continueCurrentWorkout(navigate, state.params.user), disabled: false }]);
    }
    if (!this.state.showComponent) {
      return null;
    } else {
      return (
        <View style={{flex: 1, flexDirection: 'column', justifyContent: 'space-between'}}>
          <Portal state={this.state} onchange={this.toggleRouteButton} onupdate={this.updateFormFields} />
          <RouteButtons buttons={rbuttons} />
        </View>
      );
    }
  }
};

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    flexDirection: 'row',
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  }
});

export default TimerEnhance(StatusScreen);
