import React, { Component } from 'react';
import { Dimensions, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { SwipeableFlatList } from 'react-native-swipeable-flat-list';

import OSPicker from './utils/OSHelper.js';
import GoalSettings from './utils/GoalHelper.js';
import UserSettings from './utils/UserHelper.js';
import RouteButtons from './utils/RouteHelper.js';
import WorkoutSettings from './utils/WorkoutHelper.js';

class ViewHeader extends React.Component {

  render() {
    const goalItem = this.props.state.goalData[parseInt(this.props.state.selectedGoal)];
    const dataFound = !OSPicker.isUndefinedOrNull(this.props.state.goalData) && this.props.state.goalData.length > 0 && goalItem;
    const startDate = dataFound ? OSPicker.mmddyyFormatDateTime(goalItem.sdate, '-') : '';
    const endDate = dataFound ? (OSPicker.isUndefinedOrNull(goalItem.edate) ? (goalItem.progress * 100) + '%' : OSPicker.mmddyyFormatDateTime(goalItem.edate, '-')) : '';

    return (
      <View style={{flexShrink: 1, flexDirection: 'row', height: 50, borderBottomColor: '#008000', borderBottomWidth: 0.5, justifyContent: 'center'}}>
        <Text adjustsFontSizeToFit={true} numberOfLines={1} style={{color: 'black', textAlign: 'center', textAlignVertical: 'center', justifyContent: 'space-evenly', alignSelf: 'center'}}>
          <Text>{startDate}</Text>
          <Text>{' - '}</Text>
          <Text>{endDate}</Text>
        </Text>
      </View>
    );
  }
}

class Portal extends React.Component {

  constructor(props) {
      super(props);
      this.state = {
        selectedItem: 0,
        goalItems: []
      };
      this.separatorHeight = 1;
      this.itemHeight = 50 + this.separatorHeight;
      this.dataFeedLength = parseInt((this.props.state.screenHeight - (this.props.state.footerHeight * 2)) / this.itemHeight) + 1;
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      selectedItem: nextProps.state.selectedGoal,
      goalItems: nextProps.state.goalData
    });
  }

  renderHeader = () => {
    return <Text style={styles.titem} onPress={this.getItem.bind(this, 'Header')}>{'Header'}</Text>;
  };

  renderGoals = (item, itemIndex) => {
    const colorHex = item.active ? '#ff0000' : '#000000';
    const textColor = item.active ? '#ff0000' : ((item.id == this.state.selectedItem) ? '#006400' : colorHex);
    const textTransformStyle = Object.assign(this.props.state.deviceOrientation === 'portrait' && OSPicker.getDeviceAspectRatio() <= 1.5 ? {fontSize: 9} : {display: 'flex'}, {color: item.active ? '#ff0000' : ((item.id == this.state.selectedItem) ? '#006400' : colorHex), transform: [{scaleX: 0.9}, {scaleY: 2.25}]});
    let bodyItems = [];
    if (item.options) {
      for (let i = 0; i < item.options.length; i++) {
        bodyItems = bodyItems.concat([
          <View key={i} style={{backgroundColor: (item.id == this.state.selectedItem) ? 'white' : OSPicker.hex2rgba(colorHex, 0.225), marginTop: 5, marginBottom: 5, justifyContent: 'center', alignItems: 'stretch', opacity: (item.id == this.state.selectedItem) ? 1 : 0.2}}>
            <View style={{height: 40, justifyContent: 'center', borderColor: (!item.active && item.id == this.state.selectedItem) ? '#006400' : colorHex, borderRadius: 5, borderWidth: 2, alignSelf: 'stretch'}}>
              <Text adjustsFontSizeToFit={true} style={textTransformStyle}>{item.options[i]}</Text>
            </View>
          </View>
        ]);
      }
    }
    return (
      <TouchableOpacity style={{flexShrink: 1, flexDirection: 'row', height: 50, alignItems: 'center', justifyContent: 'space-evenly', backgroundColor: (item.id == this.state.selectedItem ? '#F4FEF4' : 'transparent')}} onPress={() => { this.selectItem(item, this.props.state); }} onLongPress={() => { this.viewItem(item); }}>
        {bodyItems}
      </TouchableOpacity>
    );
  }

  renderSwipe(item, itemIndex, state) {
    const hiddenWidth = (item.id == this.state.selectedItem) ? 75 : 0;
    return (
      <Text adjustsFontSizeToFit={true} style={{flexShrink: 1, width: hiddenWidth, height: 50, textAlign: 'center', textAlignVertical: 'center', backgroundColor: '#1ef451'}} onPress={() => { this.updateItem(item, itemIndex, this.props.state); }}>{'Delete'}</Text>
    );
  }

  renderSeparator = () => {
    return (
      <View
        style={{
          height: this.separatorHeight,
          width: '100%',
          backgroundColor: '#607D8B',
        }}
      />
    );
  }

  updateItem(item, parentState) {
    if (item.id == this.state.selectedItem) {
      const msg = 'All data for this ' + (item.active ? 'CURRENT' : (item.endDate != null ? 'PREVIOUS' : 'FUTURE')) + ' workout will be deleted';
      OSPicker.showConfirmDialog('Delete Goal', msg, null, true, () => {
        let prevId = this.state.goalItems.findIndex(i => i.id === item.id) - 1;
        const prevItem = this.state.goalItems[prevId];
        prevId = prevItem ? prevId : 0;
        GoalSettings.deleteGoal(item.id).then(goals => {
          WorkoutSettings.deleteWorkout(item.id).then((workouts) => {
            UserSettings.updateUser({ workoutId: prevId }).then((user) => {
              this.setState({
                goalItems: goals,
                selectedItem: prevId
              }, () => {
                GoalSettings.updateGoal(prevId, { active: true }).then(goals => {
                  this.props.onupdate(prevId, 'selectedGoal');
                  this.props.onupdate(goals, 'goalData');
                });
              });
            });
          });
        });
      });
    }
  }

  selectItem(item, parentState) {
    this.setState({
      selectedItem: item.id
    }, () => {
      setTimeout(() => {
        this.props.onupdate(item.id, 'selectedGoal');
      }, 250);
    });
  }

  viewItem(item) {
    if (item.id == this.state.selectedItem) {
      const title = OSPicker.mdFormatDateTime(item.sdate.toString()) + ' - ' + (!OSPicker.isUndefinedOrNull(item.edate) ? OSPicker.mdFormatDateTime(item.edate.toString()) : (item.progress * 100) + '%');
      const msg = ((item.active && item.progress < 1.0) ? 'Current' : (item.edate === null ? 'Future' : 'Completed')) + ' Goal:\n\n' + item.options.map((option, index) => `${option}${(index < item.options.length - 1) ? ',' : ''}`).join(' ');
      if (item.active || item.progress >= 1.0) {
        OSPicker.showInfoDialog(title, msg);
      } else {
        OSPicker.showConfirmDialog(title, msg, ['Set Active', 'Not Now'], true, () => {
            UserSettings.updateUser({workoutId: item.id}).then(user => {
              let index = 0;
              GoalSettings.getGoals().then(goals => {
                for (let i = 0; i < goals.length; i++) {
                  goals[i].active = goals[i].id === item.id;
                  if (goals[i].active) {
                    index = i;
                  }
                }
                GoalSettings.saveGoals(goals).then(() => {
                  this.props.onupdate(item.id, 'selectedGoal');
                  this.props.onupdate(goals, 'goalData');
                  UserSettings.updateProgress(goals[index].progress);
                });
              });
            });
          });
      }
    }
  }

  getItemLayout = (data, index) => (
    {length: this.itemHeight, offset: this.itemHeight * index, index}
  )

  onLayout(initialIndex, screenWidth) {
    this.scrollView.scrollTo({x: 0, y: (initialIndex * this.itemHeight) || 0, animated: true});
  }

  render() {
    content = ( <ScrollView contentContainerStyle={styles.container}><Text>{!this.props.state.isInitialized ? 'Loading...' : 'There are no goals to view.'}</Text></ScrollView> );
    if (this.state.goalItems.length) {
      content = (
        <ScrollView ref={(scrollView) => { this.scrollView = scrollView; }} contentContainerStyle={styles.container}>
          <View style={styles.scontainer} onLayout={() => { this.onLayout(this.state.selectedItem, this.props.state.screenWidth); }}>
            <SwipeableFlatList
              ref={(swipeableFlatList) => { this.swipeableFlatList = swipeableFlatList; }}
              data={this.state.goalItems}
              containerStyle={{flex: 1}}
              initialNumToRender={this.dataFeedLength}
              showsVerticalScrollIndicator={false}
              ItemSeparatorComponent={this.renderSeparator}
              keyExtractor={item => item.id}
              renderItem={({item, index}) => this.renderGoals(item, index)}
              renderLeft={({item, index}) => this.renderSwipe(item, index, this.props.state)}
              useRefreshList={false}
              getItemLayout={this.getItemLayout}
            />
          </View>
        </ScrollView>
      );
    }
    return content;
  }
}

class GoalScreen extends React.Component {
  static navigationOptions = {
      title: 'BazzTech™️ - Dice-Fit Trainer'
  };

  constructor(props) {
      super(props);
      this.state = {
        isDisabled: false,
        showComponent: true,
        isInitialized: false,
        screenWidth: 0,
        screenHeight: 0,
        footerHeight: 30,
        selectedGoal: 0,
        deviceOrientation: OSPicker.getDeviceOrientation(),
        goalData: []
      };
      this.toggleRouteButton = this.toggleRouteButton.bind(this);
      this.updateFormFields= this.updateFormFields.bind(this);
      Dimensions.addEventListener('change', () => {
        this.setState({
          screenWidth: Dimensions.get('window').width,
          screenHeight: Dimensions.get('window').height,
          deviceOrientation: OSPicker.getDeviceOrientation()
        });
      });
  }

  componentDidMount() {
    GoalSettings.getGoals().then((goals) => {
      UserSettings.getUser(null).then(user => {
        if (goals) {
          this.setState({
            goalData: goals,
            selectedGoal: goals.findIndex(item => item.active === true),
            isInitialized: true,
          });
        }
      });
    });
  }

  componentWillMount() {
    this.setState({
      screenWidth: Dimensions.get('window').width,
      screenHeight: Dimensions.get('window').height,
      deviceOrientation: OSPicker.getDeviceOrientation()
    });
  }

  toggleRouteButton() {
    this.setState(state => {
      disabled: state.isDisabled
    });
  }

  updateFormFields(value, id1, id2 = null) {
    if (id2 === null) {
      let obj = this.state[id1];
      obj = value;
      this.setState({
        [id1]: value
      });
    } else {
      let obj = this.state[id1];
      obj[id2] = value;
      this.setState({
        [id1]: obj
      });
    }
  }

  setDisabled(hideView) {
    this.setState({
      isDisabled: true,
      showComponent: hideView ? false : true
    });
  }

  render() {
    const { state, navigate } = this.props.navigation;
    const isPortraitMode = this.state.deviceOrientation === 'portrait';
    let noteTitle = isPortraitMode ? 'Notes' : 'View Notes';
    let workoutTitle = isPortraitMode ? 'Continue Workout' : 'Continue Current Workout';
    let statusTitle = isPortraitMode ? 'Status' : 'Check Status';
    if ((isPortraitMode || this.state.screenWidth <= 480) && OSPicker.getDeviceAspectRatio() <= 1.5) {
      noteTitle = 'Notes';
      workoutTitle = isPortraitMode ? 'Workout' : 'Continue Workout';
      statusTitle = 'Status';
    }
    const rbuttons = [
        { title: noteTitle, func: () => { this.setDisabled(true); navigate("LoadScreen", {screen: "NoteScreen", user: state.params.user}); }, disabled: this.state.isDisabled },
        { title: workoutTitle, func: () => { this.setDisabled(true); navigate("LoadScreen", {screen: "WorkoutScreen", user: state.params.user}); }, disabled: this.state.isDisabled },
        { title: statusTitle, func: () => { this.setDisabled(true); navigate("LoadScreen", {screen: "StatusScreen", user: state.params.user}); }, disabled: this.state.isDisabled },
    ];
    if (!this.state.showComponent) {
      return null;
    } else {
      return (
        <View style={{flex: 1, flexDirection: 'column', justifyContent: 'space-between'}}>
          <ViewHeader state={this.state} onchange={this.toggleRouteButton} onupdate={this.updateFormFields} />
          <Portal state={this.state} onchange={this.toggleRouteButton} onupdate={this.updateFormFields} />
          <RouteButtons buttons={rbuttons} />
        </View>
      );
    }
  }
};

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    flexDirection: 'column',
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  scontainer: {
    flex: 1,
    alignSelf: 'stretch',
    backgroundColor: '#F5FCFF',
  },
  titem: {
    height: 50,
    width: '100%',
    textAlign: 'center',
    textAlignVertical: 'center',
    fontSize: 18
  }
});

export default GoalScreen;
