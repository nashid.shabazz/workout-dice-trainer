import React from 'react';
import { StackNavigator } from 'react-navigation';

import OSPicker from './utils/OSHelper.js';

import HomeScreen from './HomeScreen';
import SettingsScreen from './SettingsScreen';
import RegisterScreen from './RegisterScreen';
import ProfileScreen from './ProfileScreen';
import StatusScreen from './StatusScreen';
import WorkoutScreen from './WorkoutScreen';
import CounterScreen from './CounterScreen';
import GoalScreen from './GoalScreen';
import NoteScreen from './NoteScreen';
import LoadScreen from './LoadScreen';

const App = StackNavigator({
    HomeScreen: { screen: HomeScreen },
    SettingsScreen: { screen: SettingsScreen },
    RegisterScreen: { screen: RegisterScreen },
    ProfileScreen: { screen: ProfileScreen },
    StatusScreen: { screen: StatusScreen },
    WorkoutScreen: { screen: WorkoutScreen },
    CounterScreen: { screen: CounterScreen },
    GoalScreen: { screen: GoalScreen },
    NoteScreen: { screen: NoteScreen },
    LoadScreen: { screen: LoadScreen }
  },
  {
    initialRouteName: 'HomeScreen',
    navigationOptions: {
      title: 'BazzTech™️ - Dice-Fit Trainer',
      gesturesEnabled: false,
      headerStyle: {
        backgroundColor: '#1ef451'
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
        color: '#000000'
      },
      headerLeft: null
    },
  }
);

const prevGetStateForAction = App.router.getStateForAction;

App.router.getStateForAction = (action, state) => {
  // Do not allow to go back from Home (but allow exit from app)
  if (action.type === 'Navigation/BACK' && state && state.routes[state.index].routeName === 'HomeScreen') {
    return null;
  }

  // Do not not allow to go back from Register
  if (action.type === 'Navigation/BACK' && state && state.routes[state.index].routeName === 'RegisterScreen') {
    const newRoutes = state.routes.filter(r => r.routeName !== 'LoadScreen' && r.routeName !== 'HomeScreen');
    const newIndex = newRoutes.length - 1;

    return prevGetStateForAction(action, { index: newIndex, routes: newRoutes });
  }

  // Do not not allow to go back to Profile from Workout
  if (action.type === 'Navigation/BACK' && state && state.routes[state.index].routeName === 'WorkoutScreen') {
    const newRoutes = state.routes.filter(r => r.routeName !== 'LoadScreen' && r.routeName !== 'RegisterScreen' && r.routeName !== 'ProfileScreen');
    const newIndex = newRoutes.length - 1;

    return prevGetStateForAction(action, { index: newIndex, routes: newRoutes });
  }

  // Do not allow to go back to Home/Register unless user is on the Home/Register screen
  if (action.type === 'Navigation/BACK' && state && ['HomeScreen', 'RegisterScreen'].indexOf(state.routes[state.index].routeName) === -1) {
    const newRoutes = state.routes.filter(r => r.routeName !== 'HomeScreen' && r.routeName !== 'RegisterScreen' && r.routeName !== 'LoadScreen');
    const newIndex = newRoutes.length - 1;
    return prevGetStateForAction(action, { index: newIndex, routes: newRoutes });
  }

  return prevGetStateForAction(action, state);
};

export default App;
