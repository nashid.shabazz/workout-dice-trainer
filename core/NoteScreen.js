import React, { Component } from 'react';
import { Alert, Dimensions, Image, Keyboard, ScrollView, StyleSheet, Switch, Text, TextInput, TouchableHighlight, TouchableOpacity, View } from 'react-native';
import PopupDialog, { DialogTitle } from 'react-native-popup-dialog';
import { SwipeableFlatList } from 'react-native-swipeable-flat-list';
import TimerEnhance from 'react-native-smart-timer-enhance';
import Toast from 'react-native-smart-toast';

import Expo, { Audio } from 'expo';

import OSPicker from './utils/OSHelper.js';
import GoalSettings from './utils/GoalHelper.js';
import NoteSettings from './utils/NoteHelper.js';
import RouteButtons from './utils/RouteHelper.js';

class ViewHeader extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      toggled: false,
    };
  }

  render() {
    const filteredData = OSPicker.filter(this.props.state.noteData, 'id', String(this.props.state.selectedNote));
    const dataFound = !OSPicker.isUndefinedOrNull(this.props.state.noteData) && this.props.state.noteData.length > 0 && filteredData && filteredData.sfile;
    return (
      <View style={{flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center', borderBottomColor: '#008000', borderBottomWidth: 0.5}}>
        <Switch
          style={OSPicker.isPlatformIOS() && OSPicker.getDeviceAspectRatio() <= 1.5 ? {flex: 0.15, marginLeft: 5, marginRight: 10} : {marginLeft: 10}}
          disabled={!dataFound}
          onValueChange={(value) => { this.setState({ toggled: value }); }}
          value={this.state.toggled}
        />
        <Text adjustsFontSizeToFit={true} numberOfLines={1} style={{flex: OSPicker.isPlatformIOS() && OSPicker.getDeviceAspectRatio() <= 1.5 ? 0.75 : 1, textAlign: 'center'}}>{dataFound ? filteredData.cdate  : ''}</Text>
        <EntrySound style={{flex: 0.10}} id={'SoundRecorder'} type={'default'} canrecord={this.state.toggled} state={this.props.state} func={this.props.onchange} ufunc={this.props.onupdate} maxTimeInMs={150000} />
      </View>
    );
  }
}

class EntrySound extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      fileUri: null,
      durationMillis: 0,
      recordObject: null,
      soundObject: null,
      isDoneRecording: false,
      isRecording: false,
      isPlaying: false,
      isLoaded: false,
      haveRecordingPermissions: false
    };
  }

  componentDidMount() {
    OSPicker.askForPermissions((response) => {
      this.setState({
        haveRecordingPermissions: response.status === 'granted'
      });
    });
    setTimeout(() => {
      this.initializeAudio();
    }, 3000);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.state.selectedNote !== nextProps.state.selectedNote) {
      this.initializeAudio(nextProps);
    }
  }

  deleteAudio(parentState) {
    let data = OSPicker.filter(parentState.noteData, 'id', String(parentState.selectedNote));
    if (data && data.sfile && data.sfile.uri) {
      OSPicker.deleteSound(data.sfile.uri);
    }
  }

  initializeAudio(props) {
    const soundProps = OSPicker.isUndefinedOrNull(props) ? this.props : props;
    const filteredNote = OSPicker.filter(soundProps.state.noteData, 'id', String(soundProps.state.selectedNote));
    const soundFile = [soundProps.state.noteData, filteredNote].map(s => OSPicker.isEmpty(s)).includes(true) ? null : filteredNote.sfile;
    const soundUri = soundFile ? soundFile.uri : '';
    const onInitialize = (sound) => {
      if (this.entryView) {
        this.setState({
          soundObject: sound,
          isLoaded: true,
          isPlaying: false
        });
      }
    };

    if (this.entryView) {
      this.setState({
        fileUri: soundUri
      });
    }
    OSPicker.loadSound(new Expo.Audio.Sound(), soundUri, onInitialize, (status) => {
      if (status && status.didJustFinish) {
        this.setState({
          isPlaying: false,
          durationMillis: status.durationMillis
        });
        OSPicker.resetSound(this.state.soundObject);
      }
    }, onInitialize);
  }

  playAudio(parentState) {
    const soundFile = OSPicker.filter(parentState.noteData, 'id', String(parentState.selectedNote)).sfile;
    if (this.state.isPlaying) {
      this.pauseAudio(parentState);
    } else {
      OSPicker.playSound(this.state.soundObject, this.state.fileUri, this.state.isPlaying, this.state.isLoaded, (sound) => {
        this.setState({
          isPlaying: true
        });
      });
    }
  }

  pauseAudio(parentState) {
    OSPicker.pauseSound(this.state.soundObject, this.state.isLoaded, () => {
      this.setState({
        isPlaying: false
      });
    });
  }

  recordAudio(option, parentState, updateFunc) {
    switch (option) {
      case 'stop':
        OSPicker.endRecordSound(this.state.recordObject, this.state.isRecording, (res) => {
          this.setState({
            recordObject: null,
            fileUri: res.uri,
          });
          if (((typeof res) === 'object') && (res.uri !== 'make a recording to see its information')) {
            let uNoteData = parentState.noteData;
            OSPicker.filter(uNoteData, 'id', String(parentState.selectedNote)).sfile = res;
            updateFunc(uNoteData, 'noteData');
          }
          this.initializeAudio();
        });
        break;
      default:
      case 'start':
        OSPicker.beginRecordSound(this.state.recordObject, () => {
          this.setState({
            fileUri: null,
            recordObject: null,
          });
        }, (status) => {
          this.setState({
            isRecording: status.isRecording,
            isDoneRecording: status.isDoneRecording,
            durationMillis: status.durationMillis
          });
        }, (recording) => {
          this.setState({
            recordObject: recording,
          });
          this.deleteAudio(parentState);
        });
        break;
    }
  }

  render() {
    const filteredData = OSPicker.filter(this.props.state.noteData, 'id', String(this.props.state.selectedNote));
    const playDisabled = this.state.isRecording || OSPicker.isEmpty(this.props.state.noteData) || OSPicker.isUndefinedOrNull(filteredData) || OSPicker.isUndefinedOrNull(filteredData.sfile) || OSPicker.isUndefinedOrNull(filteredData.sfile.uri);
    const multimediaDisabled = OSPicker.isUndefinedOrNull(this.props.state.noteData) || this.props.state.noteData.length === 0 || !this.state.isLoaded;

    return (
      <View ref={(entryView) => { this.entryView = entryView; }}>
        <View style={{flexDirection: 'row'}}>
          <View pointerEvents={(playDisabled || multimediaDisabled) ? 'none' : 'auto' }>
            <TouchableOpacity
              onPress={() => { this.playAudio(this.props.state); }}>
              <Image
                style={styles.ibutton}
                source={{ uri: this.state.isPlaying ? OSPicker.getImgData().media['pause-btn-alt'] : ((playDisabled || multimediaDisabled) ? OSPicker.getImgData().media['no-play-btn'] : OSPicker.getImgData().media['play-btn-alt']) }}
              />
            </TouchableOpacity>
          </View>
          <View pointerEvents={(this.state.isPlaying || !this.props.canrecord || !this.state.haveRecordingPermissions || multimediaDisabled) ? 'none' : 'auto' }>
            <TouchableOpacity
              onPressIn={() => { this.recordAudio('start', this.props.state, this.props.ufunc); }}
              onPressOut={() => { this.recordAudio('stop', this.props.state, this.props.ufunc); }}>
              <Image
                style={styles.ibutton}
                source={{ uri: this.state.isRecording ? OSPicker.getImgData().media['stop-btn'] : ((this.state.isPlaying || !this.props.canrecord || !this.state.haveRecordingPermissions || multimediaDisabled) ? OSPicker.getImgData().media['no-record-btn'] : OSPicker.getImgData().media['record-btn-alt']) }}
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

class Portal extends React.Component {

  constructor(props) {
      super(props);
      this.state = {
        selectedItem: 0
      };
      this.separatorHeight = 1;
      this.itemHeight = 50 + this.separatorHeight;
      this.dataFeedLength = parseInt((this.props.state.screenHeight - (this.props.state.footerHeight * 2)) / this.itemHeight) + 1;
  }

  componentDidMount() {
    this.setState({
      selectedItem: this.props.state.selectedNote
    });
  }

  renderNotes = (item) => {
    return (
      <TouchableOpacity style={{height: 50, alignItems: 'center', justifyContent: 'center', backgroundColor: (item.id == this.state.selectedItem ? '#F4FEF4' : 'transparent')}} onPress={() => { this.selectItem(item, this.props.state); }} onLongPress={() => { this.viewItem(item); }}>
        <Text adjustsFontSizeToFit={true} style={{textAlign: 'center', textAlignVertical: 'center', color: (item.id == this.state.selectedItem ? '#006400' : 'black')}}>{item.note}</Text>
      </TouchableOpacity>
    );
  }

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: '100%',
          backgroundColor: '#607D8B',
        }}
      />
    );
  }

  renderSwipe(item, state) {
    let hiddenWidth = (item.id == this.state.selectedItem) ? 75 : 0;
    return (
      <Text adjustsFontSizeToFit={true} style={{width: hiddenWidth, height: 50, textAlign: 'center', textAlignVertical: 'center', backgroundColor: '#1ef451'}} onPress={() => { this.removeItem(item, this.props.state); }}>{'Delete'}</Text>
    );
  }

  removeItem(item, parentState) {
    if (item.id == this.state.selectedItem) {
      OSPicker.showConfirmDialog('Delete Note', 'Are you sure?', null, true, () => {
        const id = parseInt(item.id);
        const prevItem = (id !== 0) ? parentState.noteData[parseInt(item.id) - 1] : parentState.noteData[parseInt(item.id) + 1];
        const prevId = prevItem ? prevItem.id : '0';
        NoteSettings.deleteNote(id).then((notes) => {
          this.setState({
            selectedItem: prevId
          }, () => {
            this.props.onupdate(prevId, 'selectedNote');
            this.props.onupdate(OSPicker.filter(parentState.noteData, 'id', String(item.id), false), 'noteData');
          });
          this.props.toast.show({
            position: Toast.constants.gravity.bottom,
            children: <View><Text style={{color: '#ffffff'}}>{'Note successfully deleted!'}</Text></View>
          });
        });
      });
    }
  }

  selectItem(item, parentState) {
    this.setState({
      selectedItem: item.id
    }, () => {
      setTimeout(() => {
        this.props.onupdate(item.id, 'selectedNote');
      }, 250);
    });
  }

  viewItem(item) {
    Alert.alert(
      item.cdate,
      item.note,
      [],
      { cancelable: true }
    );
  }

  getItemLayout = (data, index) => (
    {length: this.itemHeight, offset: this.itemHeight * index, index}
  )

  render() {
    const playDisabled = this.props.state.isRecording;
    if (this.props.state.noteData.length) {
      return (
        <ScrollView
          contentContainerStyle={styles.container}
          showsVerticalScrollIndicator={false}>
          <View style={styles.scontainer}>
            <SwipeableFlatList
              data={this.props.state.noteData}
              containerStyle={{flex: 1}}
              initialNumToRender={this.dataFeedLength}
              showsVerticalScrollIndicator={false}
              ItemSeparatorComponent={this.renderSeparator}
              keyExtractor={item => item.id}
              renderItem={({item}) => this.renderNotes(item)}
              renderLeft={({item}) => this.renderSwipe(item, this.props.state)}
              useRefreshList={false}
              getItemLayout={this.getItemLayout}
            />
          </View>
        </ScrollView>
      );
    }
    return ( <ScrollView contentContainerStyle={styles.container}><Text>{!this.props.state.isInitialized ? 'Loading...' : 'There are no notes to view.'}</Text></ScrollView> );
  }
}

class NoteScreen extends React.Component {
  static navigationOptions = {
      title: 'BazzTech™️ - Dice-Fit Trainer'
  };

  constructor(props) {
      super(props);
      this.state = {
        valids: {},
        isDisabled: false,
        showComponent: true,
        isInitialized: false,
        popupVisible: false,
        hasGoal: false,
        addNoteText: '',
        keyboardSpace: 0,
        screenWidth: 0,
        screenHeight: 0,
        footerHeight: 30,
        selectedNote: 0,
        deviceOrientation: OSPicker.getDeviceOrientation(),
        noteData: []
      };
      this.toggleRouteButton = this.toggleRouteButton.bind(this);
      this.updateFormFields= this.updateFormFields.bind(this);
      Dimensions.addEventListener('change', () => {
        this.setState({
          screenWidth: Dimensions.get('window').width,
          screenHeight: Dimensions.get('window').height,
          deviceOrientation: OSPicker.getDeviceOrientation()
        });
      });
      Keyboard.addListener('keyboardDidShow', (frames) => {
        if (!frames.endCoordinates) return;
        this.setState({keyboardSpace: frames.endCoordinates.height});
      });
      Keyboard.addListener('keyboardDidHide', (frames) => {
        this.setState({keyboardSpace: 0 });
      });
  }

  componentDidMount() {
    NoteSettings.getNotes().then(notes => {
      if (notes) {
        this.setState({
          noteData: notes
        });
      }
    }).finally(() => {
      GoalSettings.getGoals().then(goals => {
        this.setState({
          isInitialized: true,
          hasGoal: goals && goals.length,
        });
      });
    });
  }

  componentWillMount() {
    this.setState({
      screenWidth: Dimensions.get('window').width,
      screenHeight: Dimensions.get('window').height,
      deviceOrientation: OSPicker.getDeviceOrientation()
    });
  }

  toggleRouteButton() {
    this.setState(state => {
      disabled: state.isDisabled
    });
  }

  updateFormFields(value, id1, id2 = null) {
    let obj = this.state[id1];
    if (id2 === null) {
      obj = value;
      this.setState({
        [id1]: value
      });
    } else {
      obj[id2] = value;
      this.setState({
        [id1]: obj
      });
    }
  }

  setDisabled(hideView) {
    this.setState({
      isDisabled: true,
      showComponent: hideView ? false : true
    });
  }

  resetDisabled() {
    this.setState({
      isDisabled: false
    }, () => {
      Keyboard.dismiss();
    });
  }

  hidePopupNoteDialog(postFunc) {
    this.popupDialog.dismiss(() => {
      postFunc();
    });
  }

  showPopupNoteDialog() {
    this.popupDialog.show();
  }

  createNote() {
    const noteString = this.state.addNoteText;
    if (!OSPicker.isEmpty(noteString)) {
      this.hidePopupNoteDialog(() => {
        this.setState({
          addNoteText: '',
        });
      });
      NoteSettings.addNote(noteString)
      .then(data => {
        this.setState({ noteData: data });
      }).catch(error => {
        console.error(error.message);
      }).finally(() => {
        Keyboard.dismiss();
      });
    }
  }

  render() {
    const { state, navigate } = this.props.navigation;
    const isPortraitMode = this.state.deviceOrientation === 'portrait';
    let goalTitle = isPortraitMode ? 'Goals' : 'Review Goals';
    let noteTitle = isPortraitMode ? 'Create Note' : 'Create New Note';
    let statusTitle = isPortraitMode ? 'Status' : 'Check Status';
    if ((isPortraitMode || this.state.screenWidth <= 480) && OSPicker.getDeviceAspectRatio() <= 1.5) {
      goalTitle = 'Goals';
      noteTitle = isPortraitMode ? 'New Note' : 'Create New Note';
      statusTitle = 'Status';
    }
    const popupDialogHeight = isPortraitMode ? 0.225 : 0.375;
    const rbuttons = [
        { title: goalTitle, func: () => { this.setDisabled(true); navigate("LoadScreen", {screen: "GoalScreen", user: state.params.user }); }, disabled: !this.state.hasGoal || this.state.isDisabled },
        { title: noteTitle, func: () => this.showPopupNoteDialog(), disabled: this.state.isDisabled },
        { title: statusTitle, func: () => { this.setDisabled(true); navigate("LoadScreen", {screen: "StatusScreen", user: state.params.user}); }, disabled: this.state.isDisabled }
    ];
    if (!this.state.showComponent) {
      return null;
    } else {
      const dialogTop = this.state.keyboardSpace ? (this.state.screenHeight / 2) + this.state.keyboardSpace - this.state.screenHeight - (!isPortraitMode && OSPicker.getDeviceAspectRatio() <= 1.5 ? 25 : 0) : (this.state.screenHeight / 2) - (this.state.screenHeight * popupDialogHeight);
      return (
        <View style={{flex: 1, flexDirection: 'column', justifyContent: 'space-between'}}>
          <PopupDialog
            ref={(popupDialog) => { this.popupDialog = popupDialog; }}
            dialogTitle={ <DialogTitle titleStyle={{backgroundColor: '#1ef451', paddingTop: 5, paddingBottom: 5}} titleTextStyle={{color: '#ffffff'}} title={noteTitle} /> }
            dialogStyle={{position: 'absolute', top: dialogTop > 0 ? dialogTop : 0}}
            dismissOnHardwareBackPress={false}
            width={0.8}
            height={popupDialogHeight}
            onShown={() => { this.setDisabled(); }}
            onDismissed={() => { this.resetDisabled(); }}>
            <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
              <View style={{margin: 5, alignSelf: 'stretch'}}>
                <TextInput
                  style={{height: 30}}
                  placeholder={'Enter note here'}
                  onChangeText={(text) => { this.setState({ addNoteText: text}); }}
                  value={this.state.addNoteText}
                />
              </View>
              <View style={{flexShrink: 1, borderRadius: 5, overflow: 'hidden'}}>
                <TouchableHighlight
                  style={styles.dbutton}
                  onPress={() => { this.createNote(); }}>
                  <Text adjustsFontSizeToFit={true} numberOfLines={1} style={{color: '#ffffff'}}>{'Add Note'}</Text>
                </TouchableHighlight>
              </View>
            </View>
          </PopupDialog>
          <ViewHeader state={this.state} onchange={this.toggleRouteButton} onupdate={this.updateFormFields} />
          <Portal state={this.state} onchange={this.toggleRouteButton} onupdate={this.updateFormFields} toast={this.toast} />
          <Toast ref={(toast) => { this.toast = toast; }} style={{marginBottom: 64, backgroundColor: 'rgba(0,128,0,0.4)'}}></Toast>
          <RouteButtons buttons={rbuttons} />
        </View>
      );
    }
  }
};

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    flexDirection: 'column',
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  scontainer: {
    flex: 1,
    alignSelf: 'stretch',
    backgroundColor: '#F5FCFF',
  },
  dbutton: {
    paddingTop: 8,
    paddingRight: 12,
    paddingBottom: 8,
    paddingLeft: 12,
    backgroundColor: '#1ef451',
  },
  ibutton: {
    width: OSPicker.getDeviceOrientation() === 'portrait' && OSPicker.getDeviceAspectRatio() <= 1.5 ? 41 : 51,
    height: OSPicker.getDeviceOrientation() === 'portrait' &&  OSPicker.getDeviceAspectRatio() <= 1.5 ? 41 : 51,
    resizeMode: Image.resizeMode.contain,
  }
});

export default TimerEnhance(NoteScreen);
