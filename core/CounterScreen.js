import React, { Component } from 'react';
import { Button, Dimensions, Image, Keyboard, ScrollView, StyleSheet, Switch, Text, TextInput, TouchableOpacity, View } from 'react-native';
import TimerEnhance from 'react-native-smart-timer-enhance';

import Timer from './utils/TimeHelper.js';
import OSPicker from './utils/OSHelper.js';
import UserSettings from './utils/UserHelper.js';
import RouteButtons from './utils/RouteHelper.js';
import WorkoutSettings from './utils/WorkoutHelper.js';

class ViewHeader extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      timerHours: '00',
      timerMinutes: '00',
      timerSeconds: '00',
      timerIsPaused: false,
      timerIsRunning: false,
      timerIsFinished: false,
      timerAlarmToggled: false,
    };
    this.trackStopWatch = this.trackStopWatch.bind(this);
  }

  disableRouting(isDisabled) {
    this.props.onupdate(isDisabled, 'isDisabled');
  }

  trackStopWatch(hrs, mins, secs) {
    this.setState({
      timerHours: hrs,
      timerMinutes: mins,
      timerSeconds: secs,
    });
  }

  pauseStopWatch() {
    this.timer.pause();
    this.setState({
      timerIsPaused: true,
      timerIsRunning: false,
      timerIsFinished: false,
    }, () => {
      this.disableRouting(false);
    });
  }

  resetStopWatch() {
    this.timer.reset();
    this.setState({
      timerHours: '00',
      timerMinutes: '00',
      timerSeconds: '00',
      timerIsPaused: false,
      timerIsRunning: false,
      timerIsFinished: false,
      timerAlarmToggled: false,
    }, () => {
      this.disableRouting(false);
    });
  }

  startStopWatch() {
    let time = [this.state.timerHours, this.state.timerMinutes, this.state.timerSeconds];
    if (this.isTimerSet() && this.isTimerFinished() && !this.isTimerAlarmSet()) {
      this.setState({
        timerHours: '00',
        timerMinutes: '00',
        timerSeconds: '00',
      });
      time = ['00','00','00'];
    }
    let func = afunc = function() {};
    if (this.isTimerSet() && this.isTimerAlarmSet()) {
      func = () => {
        this.setState({
          timerIsPaused: false,
          timerIsRunning: false,
          timerIsFinished: true,
        }, () => {
          this.disableRouting(false);
        });
      };
    }
    this.timer.start(time, func);
    this.setState({
      timerIsPaused: false,
      timerIsRunning: true,
      timerIsFinished: false,
    }, () => {
      this.disableRouting(true);
    });
  }

  stopStopWatch() {
    this.timer.stop();
    this.setState({
      timerIsPaused: false,
      timerIsRunning: false,
      timerIsFinished: true,
    }, () => {
      this.disableRouting(false);
    });
  }

  toggleStopWatch(type) {
    switch (type) {
      case 'halt':
        return this.isTimerRunning() ? this.stopStopWatch() : this.resetStopWatch();
      case 'resume':
      default:
        return this.isTimerRunning() ? this.pauseStopWatch() : this.startStopWatch();
    }
  }

  isTimerSet() {
    const isnzp = function(s) { return parseInt(s) > 0; };
    return isnzp(this.state.timerHours) || isnzp(this.state.timerMinutes) || isnzp(this.state.timerSeconds);
  }

  isTimerAlarmSet() {
    return this.state.timerAlarmToggled;
  }

  isTimerPaused() {
    return this.state.timerIsPaused;
  }

  isTimerRunning() {
    return this.state.timerIsRunning;
  }

  isTimerFinished() {
    return this.state.timerIsFinished;
  }

  formatTime(time, type) {
    return OSPicker.padZero(type.toLowerCase().indexOf('hours') > -1 ? time : Math.min(parseInt(time), 59));
  }

  getTimerButtonColor(type) {
    switch (type) {
      case 'halt':
        return this.isTimerRunning() ? '#08A82E' : '#09BA33';
      case 'resume':
      default:
        return this.isTimerRunning() ? '#0ACD39' : '#0BE03E';
    }
  }

  getTimerStatus(type) {
    switch (type) {
      case 'halt':
        return this.isTimerRunning() ? 'Stop' : 'Clear';
      case 'resume':
      default:
        return this.isTimerRunning() ? 'Pause' : (this.isTimerPaused() ? 'Resume' : 'Start');
    }
  }

  render() {
    let textFields = [];
    for (let t = 0; t < 3; t++) {
      const timeText = ['hours', 'minutes', 'seconds'].map(s => 'timer' + OSPicker.wordToProperCase(s))[t];
      textFields = textFields.concat([
        <TextInput
          key={t*3}
          style={{height: 35, marginTop: 7.5, color: this.isTimerRunning() || this.isTimerPaused() ? '#008000' : 'black', backgroundColor: (this.isTimerRunning() || !this.state.timerAlarmToggled) ? 'rgba(255,255,255,0.5)' : '#ffffff', textAlign: 'center'}}
          maxLength={2}
          placeholder={'00'}
          keyboardType={'numeric'}
          selectTextOnFocus={true}
          disabled={this.isTimerRunning()}
          onChangeText={(text) => { this.setState({ [timeText]: text}); }}
          onEndEditing={() => { this.setState({ [timeText]: this.formatTime(this.state[timeText], timeText) }) }}
          value={String(this.state[timeText])}
        />
      ]);
      if (t !== 2) {
        textFields = textFields.concat([ <Text key={t*3+1} adjustsFontSizeToFit={true} style={{textAlign: 'center', textAlignVertical: 'center', color: 'black'}}>{' : '}</Text> ]);
      }
    }
    const timerColors = ['halt', 'resume'].map(s => this.getTimerButtonColor(s));
    const timerColorStyles = timerColors.map((s, i) => Object.assign({}, OSPicker.isPlatformDroid() ? {color: timerColors[i]} : {}, {backgroundColor: timerColors[i]}));
    return (
      <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingLeft: 10, paddingRight: 10, backgroundColor: (this.state.timerAlarmToggled ? 'rgba(0,255,0,0.05)' : 'transparent'), borderBottomColor: '#008000', borderBottomWidth: 0.5}}>
        <Timer ref={(timer) => { this.timer = timer; }} alarmed={this.state.timerAlarmToggled} tfunc={this.trackStopWatch} ufunc={this.updateStopWatch} />
        <View pointerEvents={this.isTimerRunning() ? 'none' : 'auto' }>
          <Switch
            disabled={this.isTimerRunning()}
            onValueChange={(value) => { this.setState({ timerAlarmToggled: value }); }}
            value={this.state.timerAlarmToggled}
          />
        </View>
        <Button title={this.getTimerStatus('halt')} color={timerColors[0]} style={styles.button, timerColorStyles[0]} onPress={() => { Keyboard.dismiss(); this.toggleStopWatch('halt'); }} />
        <View style={{flexShrink: 1, flexDirection: 'row', height: 50, justifyContent: 'space-evenly', alignItems: 'center'}} pointerEvents={this.isTimerRunning() || !this.state.timerAlarmToggled ? 'none' : 'auto' }>
          {textFields}
        </View>
        <Button title={this.getTimerStatus('resume')} color={timerColors[1]} style={styles.button, timerColorStyles[1]} disabled={this.state.timerAlarmToggled && !this.isTimerSet()} onPress={() => { Keyboard.dismiss(); this.toggleStopWatch('resume'); }} />
      </View>
    );
  }
}


class EntryCounter extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      sets: 0,
      reps: 0,
      rolls: 0,
      counterId: null,
    };
  }

  componentWillUnmount() {
    this.clearCounterId();
  }

  onChangeForm(type, id, text, parentState, toggleButton, updateForm) {
    this.setState({ [id]: text });
    switch(type) {
      case 'numeric':
        this.onChangeNumber(id, text, parentState, type, updateForm);
        break;
      case 'text':
      default:
        this.onChangeText(id, text, type, updateForm);
        break;
    }
    parentState.isDisabled = !OSPicker.validateForm(this.formatValids(parentState.Type, Object.assign({}, parentState.valids)));
    toggleButton();
  }

  onChangeNumber(id, number, parentState, type, updateFunc) {
    updateFunc((id === 'TargetValue' && parentState.Target === 'weight' && parseInt(number) === 0) ? true : OSPicker.validateNumber(number, type), 'valids', id);
  }

  onChangeText(id, text, type, updateFunc) {
    updateFunc(text, id);
    updateFunc(OSPicker.validateText(text, id), 'valids', id);
  }

  clearCounterId() {
    if (this.state.counterId !== null) {
      clearInterval(this.state.counterId);
    }
  }

  startUpdateCounter(type, value) {
    this.setState({
      counterId: setInterval(() => {
        this.updateCounter(type, value);
      }, 300)
    });
  }

  stopUpdateCounter() {
    this.clearCounterId();
  }

  updateCounter(type, value) {
    const increment = this.state[type] + value;
    if (increment < 0 || increment > 99) {
      return;
    }
    this.setState({
      [type]: increment
    });
  }

  resetCounter(type) {
    if (this.state[type] > 0) {
      OSPicker.showConfirmDialog('Restart ' + OSPicker.wordToProperCase(type), 'Are you sure?', ['OK','Cancel'], true, () => {
         this.setState({
           [type]: 0
         });
      });
    }
  }

  render() {
    let verticalText = [];
    const percent = 1 / this.props.id.length;
    const textHeight = this.props.size / this.props.id.length;
    const textShadowStyle = {textShadowColor: 'rgba(0,128,0,0.75)', textShadowOffset: {width: -1, height: 1}, textShadowRadius: 10};
    for (let s = 0; s < this.props.id.length; s++) {
      verticalText = verticalText.concat([ <Text key={s} adjustsFontSizeToFit={true} style={{flex: percent, fontWeight: 'bold', height: textHeight, transform: [{scaleX: 1.5}]}}>{this.props.id[s].toUpperCase()}</Text> ]);
    }

    return (
      <View style={this.props.style}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <View style={{flexShrink: 1, flexDirection: 'column'}}>
            {verticalText}
          </View>
          <TouchableOpacity style={{alignSelf: 'center'}} onLongPress={() => {this.resetCounter(this.props.id); }}>
            <Text style={{fontSize: this.props.size * 2 - 20, width: this.props.size * 2 + 10, height: this.props.size * 2 + 10, color: '#ffffff', backgroundColor: '#000000', textAlign: 'center', textAlignVertical: 'center', marginLeft: 5, borderColor: '#1ef451', borderWidth: 4, borderRadius: 5}}>{this.state[this.props.id]}</Text>
          </TouchableOpacity>
          <View style={{flexDirection: 'column', marginLeft: 5, alignItems: 'center', justifyContent: 'space-between'}}>
            <TouchableOpacity style={{flexDirection: 'row', backgroundColor: this.props.color, alignItems: 'center'}} onPress={() => { this.updateCounter(this.props.id, 1); }} onPressIn={() => { this.startUpdateCounter(this.props.id, 1); }} onPressOut={() => { this.stopUpdateCounter(); }}>
              <Image style={{width: this.props.size, height: this.props.size, tintColor: '#1ef451', padding: 5, resizeMode: Image.resizeMode.contain}} source={{ uri: OSPicker.getImgData().media['counter-plus'] }} />
            </TouchableOpacity>
            <TouchableOpacity style={{flexDirection: 'row', backgroundColor: this.props.color, alignItems: 'center', justifyContent: 'space-between'}} onPress={() => { this.updateCounter(this.props.id, -1); }} onPressIn={() => { this.startUpdateCounter(this.props.id, -1); }} onPressOut={() => { this.stopUpdateCounter(); }}>
              <Image style={{width: this.props.size, height: this.props.size, tintColor: '#1ef451', padding: 5, resizeMode: Image.resizeMode.contain}} source={{ uri: OSPicker.getImgData().media['counter-minus'] }} />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

class Portal extends React.Component {

  showHelpDialog(type, value) {
    let message = '';
    switch (type) {
      case 'rollNumber':
        message = '\"Multiplier\" indicates the total number of dice rolls needed to complete the workout';
        break;
      default:
        break;
    }
    OSPicker.showHelpDialog(message);
  }

  render() {
    if (true) {
      const isPortraitMode = OSPicker.getDeviceOrientation() === 'portrait';
      const sizeAdjustment = (OSPicker.isPlatformIOS() && OSPicker.getDeviceAspectRatio() <= 1.5) ? 0.75 : 1;
      return (
        <ScrollView
          ref={(scrollView) => { this.scrollView = scrollView; }}
          contentContainerStyle={styles.container}
          showsVerticalScrollIndicator={false}>
            <View style={{flex: 0.3, flexDirection: 'row', marginTop: 10, marginLeft: 10, marginRight: 10, justifyContent: 'space-between'}}>
              <EntryCounter id='rolls' type={'default'} state={this.props.state} size={35 * sizeAdjustment} style={{alignItems: 'flex-start'}}  color={'rgba(0,255,0,0.1)'} />
              <TouchableOpacity style={{alignItems: 'flex-end', width: 65, height: 65}} onLongPress={() => { this.showHelpDialog('rollNumber', this.props.state.rollNumber); }}>
                <Text adjustsFontSizeToFit={true} style={{fontSize: ((isPortraitMode || this.props.state.screenWidth <= 480) && OSPicker.getDeviceAspectRatio() <= 1.5) ? 20 : 30, padding: 5, color: '#008000', backgroundColor: 'rgba(0,128,0,0.05)', borderColor: '#008000', borderWidth: 2, textAlign: 'center', textAlignVertical: 'center'}}>{this.props.state.rollNumber}x</Text>
              </TouchableOpacity>
            </View>
            <View style={{flex: 0.7, flexDirection: 'column', marginBottom: 10, marginRight: 10, justifyContent: 'space-between'}}>
              <EntryCounter id='sets' type={'default'} state={this.props.state} size={45 * sizeAdjustment} style={{alignSelf: 'center'}}  color={'rgba(0,255,0,0.075)'} />
              <EntryCounter id='reps' type={'default'} state={this.props.state} size={55 * sizeAdjustment} style={{alignSelf: 'flex-end'}} color={'rgba(0,255,0,0.05)'} />
            </View>
        </ScrollView>
      );
    }
    return ( <ScrollView contentContainerStyle={styles.container}><Text>{'Loading...'}</Text></ScrollView> );
  }
}

class CounterScreen extends React.Component {
  static navigationOptions = {
      title: 'BazzTech™️ - Dice-Fit Trainer'
  };

  constructor(props) {
      super(props);
      this.state = {
        valids: {},
        isDisabled: false,
        hasWorkout: false,
        showComponent: true,
        rollNumber: 0,
        keyboardSpace: 0,
        screenWidth: 0,
        screenHeight: 0,
        selectedWorkout: 0,
        deviceOrientation: OSPicker.getDeviceOrientation()
      };
      this.toggleRouteButton = this.toggleRouteButton.bind(this);
      this.updateFormFields = this.updateFormFields.bind(this);
      Dimensions.addEventListener('change', () => {
        this.setState({
          screenWidth: Dimensions.get('window').width,
          screenHeight: Dimensions.get('window').height,
          deviceOrientation: OSPicker.getDeviceOrientation()
        });
      });
      Keyboard.addListener('keyboardDidShow', (frames) => {
        if (!frames.endCoordinates) return;
        this.setState({keyboardSpace: frames.endCoordinates.height});
      });
      Keyboard.addListener('keyboardDidHide', (frames) => {
        this.setState({keyboardSpace: 0 });
      });
  }

  componentDidMount() {
    const params = this.props.navigation.state.params;
    UserSettings.getUser().then((user) => {
      WorkoutSettings.getWorkout(user.workoutId).then((workout) => {
        const workoutFound = !OSPicker.isUndefinedOrNull(workout) && workout.profile;
        this.setState({
          hasWorkout: workoutFound,
          rollNumber: !workoutFound || user.progress >= 1.0 ? 0 : ((workout && workout.schedule) ? workout.schedule.filter(item => item.active === true)[0].reps : 0)
        });
      });
    });
  }

  componentWillMount() {
    this.setState({
      screenWidth: Dimensions.get('window').width,
      screenHeight: Dimensions.get('window').height,
      deviceOrientation: OSPicker.getDeviceOrientation()
    });
  }

  toggleRouteButton() {
    this.setState(state => {
      disabled: state.isDisabled
    });
  }

  updateFormFields(value, id1, id2 = null) {
    let obj = this.state[id1];
    if (id2 === null) {
      obj = value;
      this.setState({
        [id1]: value
      });
    } else {
      obj[id2] = value;
      this.setState({
        [id1]: obj
      });
    }
  }

  setDisabled(hideView) {
    this.setState({
      isDisabled: true,
      showComponent: hideView ? false : true
    });
  }

  resetDisabled() {
    this.setState({
      isDisabled: false
    });
  }

  render() {
    const { state, navigate } = this.props.navigation;
    const isPortraitMode = this.state.deviceOrientation === 'portrait';
    let workoutTitle = isPortraitMode ? 'View Workouts' : 'Back To Workouts';
    let statusTitle = isPortraitMode ? 'Status' : 'Check Status';
    let noteTitle = isPortraitMode ? 'Notes' : 'Add Notes';
    if ((isPortraitMode || this.state.screenWidth <= 480) && OSPicker.getDeviceAspectRatio() <= 1.5) {
      statusTitle = 'Status';
      workoutTitle = isPortraitMode ? 'Workouts' : 'View Workouts';
      noteTitle = 'Notes';
    }
    const rbuttons = [
        { title: statusTitle, func: () => { this.setDisabled(); navigate("LoadScreen", {screen: "StatusScreen", user: state.params.user}); }, disabled: this.state.isDisabled },
        { title: workoutTitle, func: () => { this.setDisabled(); navigate("LoadScreen", {screen: "WorkoutScreen", user: state.params.user}); }, disabled: !this.state.hasWorkout || this.state.isDisabled },
        { title: noteTitle, func: () => { this.setDisabled(); navigate("LoadScreen", {screen: "NoteScreen", user: state.params.user}); }, disabled: this.state.isDisabled },
    ];
    if (!this.state.showComponent) {
      return null;
    } else {
      return (
        <View style={{flex: 1, flexDirection: 'column', justifyContent: 'space-between'}}>
          <ViewHeader state={this.state} onchange={this.toggleRouteButton} onupdate={this.updateFormFields} />
          <Portal state={this.state} onchange={this.toggleRouteButton} onupdate={this.updateFormFields} />
          <RouteButtons buttons={rbuttons} />
        </View>
      );
    }
  }
};

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    flexDirection: 'column',
    backgroundColor: '#fff',
    justifyContent: 'space-between',
  },
  button: {
    tintColor: '#000000',
  },
});

export default TimerEnhance(CounterScreen);
