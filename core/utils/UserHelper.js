import OSPicker from './OSHelper.js';
import GlobalStore from './StoreHelper.js';
import GlobalSettings from './SettingsHelper.js';
import WorkoutSettings from './WorkoutHelper.js';

const GS = GlobalStore;

const userObj = {
  id: 'user-' + new Date().toLocaleString().replace(/[\s:]/g, '-') + '_' + Math.random().toString().slice(2, 10),
  dob: new Date(),
  email: 'user@domain.org',
  gender: 'male', // male/female
  name: 'fullname',
  nick: 'nickname',
  height: '5\'6\"',
  weight: '163 lbs', // pounds/kilograms,
  rhr: '120',
  workoutId: 0,
  progress: 0,
  regDate: null
};

const UserSettings = {
  addUser(state) {
    userObj.id = userObj.id + '-' + state.Nickname.trim();
    userObj.dob = state.Birthdate;
    userObj.email = state.Email.trim();
    userObj.gender = state.Gender.trim();
    userObj.name = state.Fullname.trim();
    userObj.nick = state.Nickname.trim();
    userObj.height = state.Height.trim();
    userObj.weight = state.Weight.trim();
    userObj.regDate = new Date();
    return this.saveUser(userObj).then((user) =>  {
      return GlobalSettings.updateSettings({id: userObj.id, units: state.isMetric ? 'metric' : 'imperial'}).then(() => {
        return userObj;
      });
    })
    .catch(error => {
      console.error(error.message);
      return GS.emptyPromise();
    });
  },
  deleteUser() {
    return GS.deleteStore('user');
  },
  getUserAge(dob) {
    const diff_ms = Date.now() - dob.getTime();
    const age_dt = new Date(diff_ms);
    return Math.abs(age_dt.getUTCFullYear() - 1970);
  },
  getUser(obj) {
    if (obj) {
      return GlobalStore.emptyPromise(obj);
    } else {
      return GS.getStore('user').then(user => {
        if (user && !GS.isUndefinedOrNull(user.regDate)) {
          return user;
        }
        return null;
      });
    }
  },
  registerUser(data) {
    const canceledRegistration = function(status, msg = 'User registration failed!') {
      return GlobalSettings.updateSettings({isRegistered: status}).then(() => {
        return msg;
      });
    };

    return OSPicker.checkOnline().then((internetConnectivity) => {
      if (!internetConnectivity) {
        return canceledRegistration(false);
      } else {
        return fetch('https://bazztech.com/api/dice-fit/reg', {
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then((response) => response.text()).then((responseResult) => {
          return canceledRegistration(responseResult.indexOf('Error') === -1, responseResult.replace('Error:', '').trim());
        }).catch((error) => {
          return canceledRegistration(false);
        });
      }
    }).catch((error) => {
      console.error(error);
      return canceledRegistration(false);
    });
  },
  saveUser(obj) {
    return GS.saveStore('user', obj).then(() => {
      return this.getUser();
    });
  },
  updateUser(obj) {
    return GS.updateStore('user', obj).then(() => {
      return this.getUser();
    });
  },
  updateProgress(progress) {
    return this.getUser().then(user => {
      return WorkoutSettings.getWorkout(user.workoutId).then(workout => {
        return GlobalSettings.isMetricSystem().then(isMetric => {
          let newProgress = progress;
          if (workout && OSPicker.isUndefinedOrNull(progress)) {
            newProgress = user.progress;
            const wnum = parseInt(user.weight);
            const weight = isNaN(wnum) ? 0 : wnum;
            const hstr = user.height ? user.height.trim().replace(/['"]+/g, '').substring(0, 3) : '0';
            const height = isMetric ? parseInt(hstr) : parseInt(hstr[0]) * 12 + parseInt(hstr.substring(1, 3));
            switch (workout.target) {
              case 'rhr':
                const hrc = ['poor', 'belowavg', 'average', 'aboveavg', 'good', 'excellent', 'athlete'];
                const rhrStatus = OSPicker.trim(OSPicker.getEHRStatus(this.getUserAge(new Date(user.dob)), user.rhr), true).toLowerCase();
                newProgress = OSPicker.roundToFixed(hrc.indexOf(rhrStatus) / hrc.indexOf(workout.targetValue), 2);
                break;
              case 'weight':
                const weightStatus = parseInt(OSPicker.convertWeight(weight, isMetric));
                newProgress = OSPicker.roundToFixed(parseInt(OSPicker.convertWeight(workout.targetValue, isMetric)) / weightStatus, 2);
                break;
              case 'months':
              case 'weeks':
                newProgress = OSPicker.roundToFixed(workout.schedule.filter(item => item.done === true).length / workout.schedule.length, 2);
                break;
              default:
              case 'bodybuild':
                const bb = ['Curvy', 'Average', 'Toned', 'Thin'];
                const bodyBuild = OSPicker.getBodyBuild(parseFloat(OSPicker.getPI(height, weight, isMetric, false)), user.gender, isMetric);
                newProgress = OSPicker.roundToFixed(bb.indexOf(bodyBuild) / bb.indexOf(OSPicker.wordToProperCase(workout.target)), 2);
                break;
            }
          }
          return this.updateUser({ progress: newProgress }).then((user) => {
            return isFinite(newProgress) && newProgress >= 0 ? newProgress : 0;
          });
        });
      });
    });
  }
};

export default UserSettings;
