import store from 'react-native-simple-store';

const GlobalStore = {

  // Store Functions
  deleteStore(key) {
    return store.delete(key).then(() => {
      return true;
    }).catch(error => {
  		console.error(error.message);
      return false;
  	});
  },
  getStore(key) {
    return store.get(key).then(res => {
      return res;
    }).catch(error => {
      console.error(error.message);
      return null;
    });
  },
  insertStore(func) {
    return func.then(() => {
      return true;
    }).catch(error => {
      console.error(error.message);
      return false;
    });
  },
  pushStore(key, obj) {
    return this.insertStore(store.push(key, obj));
  },
  saveStore(key, obj) {
    return this.insertStore(store.save(key, obj));
  },
  updateStore(key, obj) {
    return this.insertStore(store.update(key, obj));
  },

  // Miscellaneous Functions
  emptyPromise(val = null) {
    return new Promise(resolve => {
      resolve(val);
    });
  },
  extend() {
    for (let i = 1; i < arguments.length; i++) {
      for (let key in arguments[i]) {
        if (arguments[i].hasOwnProperty(key)) {
          if (
            typeof arguments[0][key] === 'object' &&
            typeof arguments[i][key] === 'object'
          ) {
            extend(arguments[0][key], arguments[i][key]);
          } else {
            arguments[0][key] = arguments[i][key];
          }
        }
      }
    }
    return arguments[0];
  },
  isUndefinedOrNull(obj) {
    return (typeof obj === 'undefined') || (obj == null);
  },
  removeFromArray(term, arr, firstOnly = false) {
    const len = arr.length;
    for (let i = len - 1; i >= 0; i--) {
      if (arr[i] === term) {
          arr = [...arr.slice(0, i), ...arr.slice(i + 1)];
          if (firstOnly) {
            break;
          }
      }
    }
  }
}

export default GlobalStore;
