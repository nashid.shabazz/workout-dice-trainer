import OSPicker from './OSHelper.js';
import GlobalStore from './StoreHelper.js';

const GS = GlobalStore;

const workoutObj = {
  id: '-1',
  profile: 'Fit',
  type: 'Duration',
  dice: null,
  target: 'Weeks',
  targetValue: '0',
  difficulty: 'Beginner',
  progress: 0,
  sdate: null,
  edate: null,
  schedule: []
};

const WorkoutSettings = {
  addWorkout(workout) {
    let totalWorkouts = 0;
    const tdate = (new Date()).toString();
    return this.getWorkouts().then(workouts => {
      totalWorkouts = workouts.length;
    }).catch(error => {
      console.error(error.message);
    }).finally(() => {
      workoutObj.id = totalWorkouts.toString();
      workoutObj.profile = workout.profile;
      workoutObj.type = workout.type;
      workoutObj.dice = workout.dice;
      workoutObj.target = workout.target;
      workoutObj.targetValue = workout.targetValue;
      workoutObj.difficulty = workout.difficulty;
      workoutObj.sdate = OSPicker.mmddyyyyFormatDate(tdate, sep = '-') + ' [' + OSPicker.militaryFormatTime(tdate) + ']';
      workoutObj.schedule = this.generateSchedule(workoutObj);
      return GS.pushStore('workouts', workoutObj).then(() => {
        // Adding object to store
      }).catch(error => {
        console.error(error.message);
        GS.saveStore('workouts', [workoutObj]);
      }).finally(() => {
        return workoutObj;
      });
    });
  },
  deleteWorkout(id) {
    return this.getWorkouts().then(workouts => {
      return this.saveWorkouts(workouts.splice(id, 1));
    });
  },
  getWorkout(id) {
    return this.getWorkouts().then(workouts => {
      return workouts ? workouts[id] : null;
    });
  },
  getWorkouts() {
    return GS.getStore('workouts');
  },
  generateSchedule(obj) {
    let minLevel, maxLevel, totalWeeks, totalLevels, schedules = [], diceOptions = [], levelOptions = [];
    const getWorkoutDice = (cwk, options) => { return options.length === 1 ? options[0] : options[((cwk % 2 == 0) ? 1 : 0)];  };
    const getRollsPerDay = (lvl) => { return [10, 12, 15, 18, 21][lvl - 1]; };
    const getDaysPerWeek = (lvl) => { return [3, 4, 4, 5, 6][lvl - 1]; };
    const getWorkoutLevel = (cwk, twks, tls, options) => { return options[Math.min(parseInt((cwk / twks) * tls), tls - 1)]; };
    const getAdjustedLevel = (lim, val, incr) => { return ((val + incr) >= lim) ? (val + incr) : lim };
    switch (obj.profile) {
      case 'muscular':
        minLevel = 3;
        maxLevel = 5;
        break;
      case 'slim':
        minLevel = 1;
        maxLevel = 3;
        break;
      case 'fit':
        minLevel = 2;
        maxLevel = 4;
        break;
    }
    switch (obj.target) {
      case 'weeks':
        totalWeeks = parseInt(obj.targetValue);
        break;
      case 'months':
        totalWeeks = parseInt(obj.targetValue) * 4;
        break;
      case 'rhr':
      case 'weight':
      case 'bodybuild':
      default:
        totalWeeks = 52;
        break;
    }
    // DWPLevel⩽ DDLevel
    switch (obj.difficulty) {
      case 'expert': // Level I - V
        diceOptions = ['home','gym'];
        minLevel = minLevel;
        maxLevel = maxLevel;
        break;
      case 'trainee': // Level I - IV
        diceOptions = ['gym'];
        minLevel = getAdjustedLevel(1, minLevel, -1);
        maxLevel = getAdjustedLevel(1, maxLevel, -1);
        break;
      case 'beginner': // Level I - III
        diceOptions = ['home'];
        minLevel = getAdjustedLevel(1, minLevel, -2);
        maxLevel = getAdjustedLevel(1, maxLevel, -2);
      default:
        break;
    }
    switch (obj.type) {
      case 'custom':
        diceOptions = obj.dice === 'combo' ? ['home','gym'] : [obj.dice];
        break;
      case 'duration':
      case 'bodybuild':
      default:
        break;
    }
    for (let l = minLevel; l <= maxLevel; l++) {
      levelOptions = levelOptions.concat([l]);
    }
    totalLevels = levelOptions.length;
    for (let currentWeek = 1; currentWeek <= totalWeeks; currentWeek++) {
      const currentLevel = getWorkoutLevel(currentWeek, totalWeeks, totalLevels, levelOptions);
      const currentDice = getWorkoutDice(currentWeek, diceOptions);
      const daysPerWeek = getDaysPerWeek(currentLevel);
      const rollsPerDay = getRollsPerDay(currentLevel);
      for (let currentDay = 1; currentDay <= daysPerWeek; currentDay++) {
        const workoutId = (schedules.length + 1).toString();
        const isActive = currentDay === 1 && currentWeek === 1;
        schedules = schedules.concat([{ id: workoutId, active: isActive, done: false, week: currentWeek, day: currentDay, level: currentLevel, reps: rollsPerDay, dice: currentDice }]);
      }
    }
    return schedules;
  },
  saveWorkouts(arr) {
    return GS.saveStore('workouts', arr).then(() => {
      return this.getWorkouts();
    });
  },
  updateWorkout(id, obj) {
    return this.getWorkouts().then(workouts => {
      workouts[id] = Object.assign(workouts[id], obj);
      return this.saveWorkouts(workouts);
    });
  },
};

export default WorkoutSettings;
