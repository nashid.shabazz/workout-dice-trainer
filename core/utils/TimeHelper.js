import React, { Component } from 'react';
import { Alert, View } from 'react-native';

import Expo, { Audio } from 'expo';

import OSPicker from '../utils/OSHelper.js';

class Timer extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      timer: null,
      isRunning: false,
      isFinished: false,
      isPaused: false,
      counter: 0,
    };
  }

  componentWillUnmount() {
    this.clearTimer();
  }

  clearTimer() {
    this.clearInterval(this.state.timer);
    this.setState({ timer: null });
  }

  hours() {
    return OSPicker.padZero(Math.floor(this.getTimerSeconds() / 3600));
  }

  minutes() {
    return OSPicker.padZero(Math.floor((this.getTimerSeconds() % 3600) / 60));
  }

  seconds() {
    return OSPicker.padZero(this.getTimerSeconds() % 3600 % 60);
  }

  getTimerSeconds() {
    return this.state.counter;
  }

  tick() {
    this.setState({
      counter: this.getTimerSeconds() + (this.isAlarmSet() ? -1 : 1)
    }, () => {
      this.props.tfunc(this.hours(), this.minutes(), this.seconds());
    });
  }

  start(time, pfunc) {
    let func = function() {};
    if (!this.isRunning()) {
      if (this.isAlarmSet()) {
        this.setState({
          counter: this.isPaused() ? this.getTimerSeconds() : parseInt(time[0]) * 3600 + parseInt(time[1]) * 60 + parseInt(time[2])
        });
        func = () => {
          if (this.getTimerSeconds() <= 0)
          {
            this.clearTimer();
            const alarmSound = new Expo.Audio.Sound();
            OSPicker.playFile(alarmSound, require('../assets/sound/alarm.mp3'), true, true);
            Alert.alert(
              'Counter Alarm',
              'Time is up!',
              [
                {text: 'OK', onPress: () => { OSPicker.resetSound(alarmSound); this.stop(); }},
              ],
              { cancelable: false }
            );
            pfunc();
          }
        };
      } else {
        this.setState({
          counter: this.isPaused() ? this.getTimerSeconds() : 0,
        });
      }
      this.setState({
        isPaused: false,
        isRunning: true,
        timer: setInterval(() => { this.tick(); if (func) func(); }, 1000)
      });
    }
  }

  pause(pfunc) {
    if (this.isRunning()) {
      this.clearTimer();
      if (pfunc) {
        pfunc();
      } else {
        this.setState({
          isPaused: true,
          isRunning: false,
        });
      }
    }
  }

  stop() {
    this.pause(() => {
      this.setState({
        isPaused: false,
        isRunning: false,
      });
    });
  }

  reset() {
    if (!this.isRunning()) {
      this.setState({
        timer: null,
        counter: 0,
        isPaused: false,
        isRunning: false,
        isFinished: false,
        alarmSet: false
      });
    }
  }

  isAlarmSet() {
    return this.props.alarmed;
  }

  setAlarm(status) {
    if (!this.isRunning()) {
      this.setState({
        alarmSet: status
      });
    }
  }

  isRunning() {
    return this.state.isRunning;
  }

  isFinished() {
    return this.isAlarmSet() && this.state.counter === 0;
  }

  isPaused() {
    return this.state.isPaused;
  }

  render() {
    return (
      <View style={{display: 'none'}} />
    );
  }
}

export default TimerEnhance(Timer);
