import OSPicker from './OSHelper.js';
import GlobalStore from './StoreHelper.js';

const GS = GlobalStore;

const goalObj = {
  id: '-1',
  active: false,
  sdate: null,
  edate: null,
  progress: 0,
  options: []
};

const GoalSettings = {
  addGoal(goal) {
    let totalGoals = 0;
    const tdate = (new Date()).toString();
    const todayDateFormatted = OSPicker.mmddyyyyFormatDate(tdate, sep = '-') + ' [' + OSPicker.militaryFormatTime(tdate) + ']';
    return this.getGoals().then(goals => {
      totalGoals = goals.length;
      if (goal.active && totalGoals > 0) {
        for (let i = 0; i < totalGoals; i++) {
          goals[i].active = false;
        }
        return GoalSettings.saveGoals(goals);
      }
    }).catch(error => {
      console.error(error.message);
    }).finally(() => {
      goalObj.id = totalGoals.toString();
      goalObj.active = goal.active;
      goalObj.sdate = tdate;
      goalObj.options = OSPicker.objectToArray(goal.workout.options).map(item => {
        const arr = ['aboveavg', 'belowavg', 'bodybuild'];
        const newArr = ['Above Avg', 'Below Avg', 'Body Build'];
        const i = arr.indexOf(item);
        return (i < 0) ? OSPicker.wordToProperCase(item) : newArr[i];
      });
      return GS.pushStore('goals', goalObj)
      .catch(error => {
        console.error(error.message);
        GS.saveStore('goals', [goalObj]);
      }).finally(() => {
        return goalObj;
      });
    });
  },
  deleteGoal(id) {
    return this.getGoals().then(goals => {
      return this.saveGoals(goals.splice(id, 1));
    });
  },
  getGoal(id) {
    return this.getGoals().then(goals => {
      return goals ? goals[id] : null;
    });
  },
  getGoals() {
    return GS.getStore('goals');
  },
  updateGoal(id, obj) {
    return this.getGoals().then(goals => {
      goals[id] = Object.assign(goals[id], obj);
      return this.saveGoals(goals);
    });
  },
  saveGoals(arr) {
    return GS.saveStore('goals', arr).then(() => {
      return this.getGoals();
    });
  },
};

export default GoalSettings;
