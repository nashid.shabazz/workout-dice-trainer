import React from 'react';
import { Alert, Dimensions, Platform } from 'react-native';

import Expo, { Audio, FileSystem, Permissions } from 'expo';

import GlobalStore from './StoreHelper.js';

const GS = GlobalStore;
const imgData = require('../imgbase64.json');

const OSPicker = {
  async askForPermissions(postFunc) {
    const response = await Permissions.askAsync(Permissions.AUDIO_RECORDING);
    postFunc(response);
  },
  async beginRecordSound(recordObject, initFunc, updateFunc, postFunc) {
      try {
        if (recordObject) {
          recordObject.setOnRecordingStatusUpdate(null);
          initFunc();
        }
        await this.setAudioMode({ allowsRecordingIOS: true });
        const recording = new Audio.Recording();
        recording.setOnRecordingStatusUpdate((status) => {
          updateFunc(status);
        });
        recording.setProgressUpdateInterval(200);
        await recording.prepareToRecordAsync(Audio.RECORDING_OPTIONS_PRESET_HIGH_QUALITY);
        await recording.startAsync();
        postFunc(recording);
      } catch (error) {
        console.error('beginRecordSound', error);
      }
  },
  centimetersToInches(centimeters) {
    return centimeters * 0.3937007874;
  },
  centimetersToMeters(centimeters) {
    return centimeters * 0.01;
  },
  checkOnline() {
    return new Promise((resolve, reject) => {
      let xhr = new XMLHttpRequest();
      xhr.open('GET', 'https://www.bazztech.com', true);
      xhr.timeout = 3000;
      xhr.onreadystatechange = (e) => {
        if (xhr.readyState === XMLHttpRequest.DONE) {
          if (xhr.status === 200) {
            resolve(true);
          } else {
            reject(false);
          }
        }
      };

      xhr.ontimeout = (e) => {
        reject('timeout');
      };

      xhr.send(null);

      setTimeout(() => {
        if (xhr.readyState !== XMLHttpRequest.DONE) {
          xhr.abort();
          reject('timeout');
        }
      }, xhr.timeout);
    });
  },
  convertHeight(text, isMetric) {
    let height = text.toString().trim().replace(/['"]+/g, '').substring(0, 3);
    if (this.getLastWord(text) === 'cm') {
      const inches = Math.round(this.centimetersToInches(parseInt(height)));
      height = isMetric ? text : (parseInt(inches / 12) + '\'' + (inches % parseInt(inches / 12)) + '\"');
    } else {
      height = isMetric ? Math.round(this.inchesToCentimeters((parseInt(height[0]) * 12) + parseInt(height.substring(1, 3)))) : text;
    }
    return height;
  },
  convertWeight(text, isMetric) {
    let weight = text.toString().trim().substring(0, 3);
    if (this.getLastWord(text) === 'kgs') {
      weight = isMetric ? text : parseInt(this.kilogramsToPounds(parseInt(weight)));
    } else {
      weight = isMetric ? parseInt(this.poundsToKilograms(parseInt(weight))) : text;
    }
    return weight;
  },
  async deleteSound(soundUri, postFunc) {
    try {
      const info = await FileSystem.deleteAsync(soundUri, { idempotent: false });
      postFunc(soundUri);
    } catch (error) {
      console.error('deleteSound', error);
    }
  },
  async endRecordSound(recordObject, isRecording, postFunc) {
    try {
      await this.sleep(500);
      await recordObject.stopAndUnloadAsync();
      await this.setAudioMode({ allowsRecordingIOS: false });
    } catch (error) {
      console.error('endRecordSound', error);
    }

    if (recordObject) {
      const fileUrl = recordObject.getURI();
      recordObject.setOnRecordingStatusUpdate(null);
      postFunc({uri: fileUrl});
    }
  },
  filter(data, type, match, isEqual = true) {
    let newData = [];
    for (let i = 0; i < data.length; i++) {
      if (data[i][type] == match) {
        if (isEqual) {
          return data[i];
        }
      } else if (!isEqual) {
        newData = newData.concat([data[i]]);
      }
    }
    if (!isEqual) {
      return newData;
    }
  },
  formatHeight(text, isMetric) {
    text = text.toString().trim().replace(/['"]+/g, '').substring(0, 3);
    if (isMetric) {
      return this.getNumberLimit(text, 30.48, 241.3) + ' ' + this.getUnits('height', isMetric);
    } else {
      const feet = this.getNumberLimit(text[0], 1, 7);
      switch (text.length) {
        case 0:
          return text;
        case 1:
          return  feet + '\'0\"';
        case 2:
          return  feet + '\'' + text[1] + '\"';
        case 3:
        default:
          return  feet + '\'' + this.getNumberLimit(text.slice(1,3), 0, 11) + '\"';
      }
    }
  },
  formatRHR(text) {
    return this.getNumberLimit(text, 20, 120).toString();
  },
  formatWeight(text, isMetric, isInteger = false) {
    text = text.toString().trim().substring(0, 4);
    if (isMetric) {
      const fpNumber = this.getNumberLimit(text, 11.3398, 453.139);
      return (!Number.isInteger(fpNumber) ? (isInteger ? parseInt(fpNumber) : fpNumber.toFixed(1)) : fpNumber) + ' ' + this.getUnits('weight', isMetric);
    }
    return this.getNumberLimit(text, 25, 999) + ' ' + this.getUnits('weight', isMetric);
  },
  getBMI(height, weight, isMetric, showUnits = true) {
    if (isMetric) {
      return this.roundToFixed((weight / Math.pow(this.centimetersToMeters(height), 2)), 1) + (showUnits ? ' ' + this.getUnits('bmi', isMetric) : '');
    } else {
      return this.roundToFixed((703.0704 * weight / Math.pow(height, 2)), 1) + (showUnits ? ' ' + this.getUnits('bmi', isMetric) : '');
    }
  },
  getBodyBuild(pi, gender, isMetric) {
    const incr = gender.toLowerCase() === 'male' ? -0.5 : 0;
    switch (true) {
      case (pi <= (11.5 + incr)): // 11.5 or lower
        return isMetric ? 'Thin' : 'Curvy';
      case (pi <= (13.0 + incr)): // 11.5 < PI <= 13.0
        return isMetric ? 'Toned' : 'Average';
      case (pi <= (14.5 + incr)): // 13.0 < PI <= 14.5
        return isMetric ? 'Average' : 'Toned';
      case (pi > (14.5 + incr)): // 14.5 or higher
      default:
        return isMetric ? 'Curvy' : 'Thin';
    }
  },
  getDeviceAspectRatio() {
    const {width, height} = this.getDeviceDimensions();
    return height / width;
  },
  getDeviceDimensions() {
    return Dimensions.get('window');
  },
  getDeviceOrientation() {
    const {width, height} = this.getDeviceDimensions();
    return (height >= width) ? 'portrait' : 'landscape';
  },
  getEHRStatusByPercentile(per, rhr) {
    const categories = ['Athlete', 'Excellent', 'Good', 'Above Avg', 'Average', 'Below Avg', 'Poor'];
    const ranges = [
      [[55], [55, 61], [61, 65], [65, 69], [69, 73], [73, 80], [80]],
      [[54], [54, 61], [61, 65], [65, 70], [70, 74], [74, 80], [80]],
      [[56], [56, 62], [62, 66], [66, 70], [70, 75], [75, 81], [81]],
      [[57], [57, 63], [63, 67], [67, 71], [71, 76], [76, 82], [82]],
      [[56], [56, 62], [62, 67], [67, 71], [71, 75], [75, 79], [79]],
      [[55], [55, 61], [61, 65], [65, 70], [70, 74], [74, 77], [77]]
    ];
    const getStatus = function(p, r) {
      const range = ranges[p];
      for (let i = 0; i < range.length; i++) {
        if (i === 0 && r < range[i][0]) {
          return categories[i];
        } else if (i === (range.length - 1) && r >= range[i][0]) {
          return categories[i];
        } else if (r >= range[i][0] && r < range[i][1]) {
          return categories[i];
        }
      }
      return categories[range.length - 1];
    };
    return getStatus(per - 1, rhr);
  },
  getEHRStatus(age, rhr) {
    let percentile;
    switch (true) {
      case (age < 26): // 18-25 (or lower age)
        percentile = 1;
        break;
      case (age < 36): // 26-35
        percentile = 2;
        break;
      case (age < 46): // 36-45
        percentile = 3;
        break;
      case (age < 56): // 46-55
        percentile = 4;
        break;
      case (age < 66): // 56-65
        percentile = 5;
        break;
      default: // 65+
        percentile = 6;
      break;
    }
    return this.getEHRStatusByPercentile(percentile, parseInt(rhr));
  },
  getGreeting() {
    const today = new Date();
    const curHour = today.getHours();
    if (curHour < 12) {
      return 'Good morning!';
    } else if (curHour < 18) {
      return 'Good afternoon!';
    } else {
      return 'Good evening!';
    }
  },
  getImgData() {
    return imgData;
  },
  getLastWord(text, delim = ' ') {
    return text.split(delim).pop().trim();
  },
  getMaxInteger() {
    return Math.pow(2, 31);
  },
  getNumberLimit(text, min, max) {
    const num = parseInt(text);
    const val = isNaN(num) ? min : num;
    return Math.min(Math.max(val, min), max);
  },
  getPI(height, weight, isMetric, showUnits = true) {
    if (isMetric) {
      return this.roundToFixed((weight / Math.pow(this.centimetersToMeters(height), 3)), 1) + (showUnits ? ' ' + this.getUnits('pi', isMetric) : '');
    } else {
      return this.roundToFixed((height / Math.cbrt(weight)), 1) + (showUnits ? ' ' + this.getUnits('pi', isMetric) : '');
    }
  },
  getUnits(type, isMetric) {
    switch (type.toLowerCase()) {
      case 'bmi':
        return isMetric ? 'kg/m²' : 'lb/in²';
      case 'height':
        return isMetric ? 'cm' : 'in';
      case 'pi':
        return isMetric ? 'kg/m³' : 'in/³√lb';
      case 'weight':
        return isMetric ? 'kgs' : 'lbs';
      default:
        return '';
    }
  },
  getVersion() {
    return 'v1.0';
  },
  hex2rgba(hex, a) {
    let c;
    if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
      c = hex.substring(1).split("");
      if (c.length === 3) {
        c = [c[0], c[0], c[1], c[1], c[2], c[2]];
      }
      c = "0x" + c.join("");
      return (
        "rgba(" +
        [(c >> 16) & 255, (c >> 8) & 255, c & 255].join(",") +
        ", " +
        a +
        ")"
      );
    }
    return "";
  },
  inchesToCentimeters(inches) {
    return inches * 2.54;
  },
  isEmpty(obj) {
    return this.isUndefinedOrNull(obj) || obj.length == 0 || (typeof obj === 'string' && obj.trim() === '');
  },
  isPlatformDroid() {
    return !this.isPlatformIOS();
  },
  isPlatformIOS() {
    return Platform.OS === 'ios';
  },
  isUndefinedOrNull(obj) {
    return GS.isUndefinedOrNull(obj);
  },
  kilogramsToPounds(kgs) {
    return kgs * 2.2046226218;
  },
  async loadSound(soundObject, soundUri, initFunc, updateFunc, postFunc) {
    if (soundObject) {
      await soundObject.unloadAsync();
      soundObject.setOnPlaybackStatusUpdate(null);
      initFunc(null);
    }
    const sound = new Expo.Audio.Sound();
    sound.setOnPlaybackStatusUpdate(updateFunc);
    if (soundUri) {
      await sound.loadAsync({ uri: soundUri }).then(() => {
        postFunc(sound);
      });
    } else {
      postFunc(sound);
    }
  },
  metersToInches(meters) {
    return meters * 39.3700787402;
  },
  militaryFormatTime(text, sep = ':') {
    const date = new Date(text);
    const h = this.padZero(date.getHours());
    const m = this.padZero(date.getMinutes());
    const s = this.padZero(date.getSeconds());
    return h + sep + m + sep + s;
  },
  mdFormatDateTime(text, sep = '/') {
    const date = new Date(text);
    if (!GS.isUndefinedOrNull(date)) {
      const month = (1 + date.getMonth()).toString();
      const day = date.getDate().toString();
      const time24 = this.militaryFormatTime(text).slice(0, -3);
      return month + sep + day  + ` [${time24}]`;
    }
    return date.toString();
  },
  mddyyFormatDateTime(text, sep = '/') {
    const mddyyDate = this.mddyyyyFormatDate(text, sep);
    const time24 = this.militaryFormatTime(text).slice(0, -3);
    return mddyyDate.slice(0, -4) + mddyyDate.slice(-2) + ` [${time24}]`;
  },
  mmddyyFormatDateTime(text, sep = '/') {
    const mmddyyDate = this.mmddyyyyFormatDate(text, sep);
    const time24 = this.militaryFormatTime(text).slice(0, -3);
    return mmddyyDate.slice(0, -4) + mmddyyDate.slice(-2) + ` [${time24}]`;
  },
  mddyyyyFormatDate(text, sep = '/') {
    const dateString = this.mmddyyyyFormatDate(text, sep);
    return ((parseInt(dateString.substring(0, 2)) < 10) ? dateString[1] : dateString.substring(0, 2)) + dateString.substring(2);
  },
  mmddyyyyFormatDate(text, sep = '/') {
    const date = new Date(text);
    if (!GS.isUndefinedOrNull(date)) {
      const year = date.getFullYear();
      let month = (1 + date.getMonth()).toString();
      month = month.length > 1 ? month : '0' + month;
      let day = date.getDate().toString();
      day = day.length > 1 ? day : '0' + day;
      return month + sep + day + sep + year;
    }
    return date.toString();
  },
  mmddyyyyFormatDateYYIncr(text, incr, sep) {
    const date = new Date(text);
    date.setFullYear(date.getFullYear() + incr);
    return this.mmddyyyyFormatDate(date.toString(), sep);
  },
  nextQueueValue(arr, val) {
    const index = arr.indexOf(val);
    return (index < 0 || index == arr.length - 1) ? arr[0] : arr[index + 1];
  },
  numberToRomanNumeral(text) {
    const number = parseInt(text);
    const romans = ['I','II','III','IV','V'];
    return romans[Math.min(romans.length - 1, Math.max(0, number-1))];
  },
  objectToArray(obj, includeNulls = false) {
    let arr = [];
    for (let o in obj) {
      if (includeNulls || (!this.isUndefinedOrNull(obj[o]) && obj[o].toString().trim() !== '0')) {
        arr = arr.concat([obj[o].toString()]);
      }
    }
    return arr;
  },
  padZero(i) {
    return (i < 10) ? ('0' + i) : i;
  },
  async pauseSound(soundObject, isLoaded, postFunc) {
    if (isLoaded) {
      soundObject.pauseAsync().then(() => {
        postFunc();
      });
    }
  },
  async playFile(soundObject, soundUri, isLooping, isLocal = false) {
    if (!soundObject) {
      const sound = new Expo.Audio.Sound();
      soundObject = sound;
    }
    const soundFile = isLocal ? soundUri : { uri: soundUri };
    await soundObject.loadAsync(soundFile).then((res) => {
      soundObject.setIsLoopingAsync(isLooping);
      soundObject.playAsync();
    });
  },
  async playSelectSound(type) {
    try {
      switch (type) {
        case 'lock':
          this.playFile(null, require('../assets/sound/lock.mp3'), false, true);
          break;
        case 'tick':
        default:
          this.playFile(null, require('../assets/sound/tick.mp3'), false, true);
          break;
      }
    } catch (error) {
      console.error('playSelectTick', error);
    }
  },
  async playSound(soundObject, soundUri, isPlaying, isLoaded, postFunc) {
    const startPlay = function() {
      soundObject.playAsync().then(() => {
        postFunc();
      });
    };
    if (isLoaded && !isPlaying && soundObject) {
      soundObject.getStatusAsync().then((status) => {
        if (!status.isLoaded) {
          soundObject.loadAsync({ uri: soundUri }).then(() => {
            startPlay();
          });
        } else {
          startPlay();
        }
      });
    }
  },
  async playStatusComplete(isFirstPlay = false) {
    try {
      const sound = new Expo.Audio.Sound();
      sound.setOnPlaybackStatusUpdate((status) => {
        if (status && status.didJustFinish) {
          this.resetSound(sound).then(() => {
            if (isFirstPlay) {
              this.playFile(null, require('../assets/sound/applause.mp3'), false, true);
            }
          });
        }
      });
      if (isFirstPlay) {
        this.playFile(sound, require('../assets/sound/opera.mp3'), false, true);
      } else {
        this.playFile(null, require('../assets/sound/sparkles.mp3'), false, true);
      }
    } catch (error) {
      console.error('playStatusComplete', error);
    }
  },
  poundsToKilograms(pounds) {
    return pounds * 0.45359237;
  },
  radiansToDegrees(radians) {
    return radians * 180 / Math.PI;
  },
  async recordSound(recordObject, soundObject, isComplete, postFunc) {
    if (!isComplete) {
      // StopPlaybackAndBeginRecording
      try {
        if (recordObject) {
          await this.sleep(400);
          await recordObject.stopAndUnloadAsync();
          await Audio.setAudioModeAsync({ allowsRecordingIOS: false });

          recordObject.setOnRecordingStatusUpdate(null);
          recordObject = null;
        }
      } catch (error) {
        console.error('STOP recordSound', error);
      }
      await Audio.setAudioModeAsync({
        allowsRecordingIOS: true,
        interruptionModeIOS: Audio.INTERRUPTION_MODE_IOS_DO_NOT_MIX,
        playsInSilentModeIOS: true,
        shouldDuckAndroid: true,
        interruptionModeAndroid: Audio.INTERRUPTION_MODE_ANDROID_DO_NOT_MIX
      });
      const recording = new Audio.Recording();
      recording.setOnRecordingStatusUpdate(() => (status) => {
        postFunc(status);
      });
      recording.setProgressUpdateInterval(200);
      await recording.prepareToRecordAsync(Audio.RECORDING_OPTIONS_PRESET_HIGH_QUALITY);
      await recording.startAsync();
    } else {
      // stopRecordingAndEnablePlayback
      try {
        await this.sleep(400);
        await recordObject.stopAndUnloadAsync();
        await Audio.setAudioModeAsync({ allowsRecordingIOS: false });
      } catch (error) {
        console.error('STOP recordSound', error);
      }
      if (recordObject) {
        const fileUrl = recordObject.getURI();
        const info = await FileSystem.getInfoAsync(fileUrl);
        recordObject.setOnRecordingStatusUpdate(null);
        postFunc(info);
      }
    }
  },
  async resetSound(soundObject) {
    if (!this.isUndefinedOrNull(soundObject)) {
      await soundObject.stopAsync();
    }
  },
  roundToFixed(val, prec) {
    const mult = Math.pow(10, prec || 0);
    return Math.round(val * mult) / mult;
  },
  async setAudioMode({allowsRecordingIOS}) {
    try {
      await Audio.setAudioModeAsync({
        allowsRecordingIOS,
        interruptionModeIOS: Audio.INTERRUPTION_MODE_IOS_DO_NOT_MIX,
        playsInSilentModeIOS: true,
        shouldDuckAndroid: true,
        interruptionModeAndroid: Audio.INTERRUPTION_MODE_ANDROID_DO_NOT_MIX,
      });
    } catch (error) {
      console.error('setAudioMode', error);
    }
  },
  showConfirmDialog(title, msg, options, isModal, sfunc, nfunc = () => { return; }) {
    Alert.alert(
      title,
      msg,
      [
        {text: options && options[0] ? options[0] : 'Yes', onPress: () => { sfunc(); }},
        {text: options && options[1] ? options[1] : 'No', onPress: () => { nfunc(); }, style: 'cancel'},
      ],
      { cancelable: !isModal }
    );
  },
  showInfoDialog(title, msg, isModal = false) {
    Alert.alert(
      title,
      msg,
      [],
      { cancelable: !isModal }
    );
  },
  showHelpDialog(msg, title = 'Reference', sfunc = () => { return; }, isModal = false) {
    Alert.alert(
      title,
      msg,
      [
        {text: 'OK', onPress: () => { sfunc(); }},
      ],
      { cancelable: !isModal }
    );
  },
  sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  },
  splitProperNouns(key, sep = ' ') {
    return key.match(/[A-Z][a-z]+/g).join(sep);
  },
  trim(str, removeAll = false) {
    if (removeAll) {
      return str.replace(/\s/g, '');
    }
    return str.trim();
  },
  validateDate(date) {
    return date instanceof Date && !isNaN(date.valueOf());
  },
  validateEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  },
  validateForm(valids) {
    let valid = true;
    for (let key in valids) {
      if (!valids[key]) {
        valid = false;
        break;
      }
    }
    return valid;
  },
  validateNumber(number, type) {
    return !(isNaN(number) && isNaN(parseInt(number))) && parseInt(number) > 0;
  },
  validateText(text, type) {
    if (text && text.length) {
      switch (type) {
        case 'Fullname':
          return text.trim().split(' ').filter(function (v) { return v !== ''} ).length > 1;
        case 'Nickname':
        default:
          return text.trim().length > 2;
      }
    }
    return false;
  },
  wordToProperCase(word) {
    return word.toString().replace(/\w\S*/g, function(t) { return t.charAt(0).toUpperCase() + t.substr(1).toLowerCase(); });
  }
};

export default OSPicker;
