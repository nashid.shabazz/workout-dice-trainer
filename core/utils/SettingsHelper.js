import GlobalStore from './StoreHelper.js';

const GS = GlobalStore;

const settingsObj = {
  id: null,
  isRegistered: false,
  units: 'imperial'
};

const GlobalSettings = {
  initSettings(obj) {
    return this.getSettings().then(settings => {
      return settings;
    })
    .catch(error => {
      console.error(error.message);
      if (GS.isUndefinedOrNull(obj)) {
        obj = settingsObj;
      }
      return this.saveSettings(Object.assign(settingsObj, obj)).then(() => {
        return obj;
      });
    });
  },
  getSettings() {
    return GS.getStore('settings');
  },
  isMetricSystem(units) {
    if (units) {
      return GS.emptyPromise(units === 'metric');
    } else {
      return this.getSettings().then(settings => {
        return settings && settings.units === 'metric';
      });
    }
  },
  saveSettings(obj) {
    return GS.saveStore('settings', obj).then(() => {
      return this.getSettings();
    });
  },
  updateSettings(obj) {
    return GS.updateStore('settings', obj).then(() => {
      return this.getSettings();
    });
  }
};

export default GlobalSettings;
