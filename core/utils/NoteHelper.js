import OSPicker from './OSHelper.js';
import GlobalStore from './StoreHelper.js';

const GS = GlobalStore;

const noteObj = {
  id: '-1',
  cdate: new Date().toString(),
  note: '',
  sfile: {} // sound file info
};

const NoteSettings = {
  addNote(note) {
    let newNotes = [];
    let totalNotes = 0;
    const tdate = (new Date()).toString();
    return this.getNotes().then(notes => {
      newNotes = notes.slice();
      totalNotes = notes.length;
    }).catch(error => {
      console.error(error.message);
    }).finally(() => {
      noteObj.id = totalNotes.toString();
      noteObj.cdate = OSPicker.mmddyyyyFormatDate(tdate, sep = '-') + ' [' + OSPicker.militaryFormatTime(tdate) + ']';
      noteObj.note = note;
      return GS.pushStore('notes', noteObj).then(() => {
        // Adding object to store
        newNotes = newNotes.concat([noteObj]);
      }).catch(error => {
        console.error(error.message);
        GS.saveStore('notes', [noteObj]);
      }).finally(() => {
        return newNotes;
      });
    });
  },
  getNotes() {
    return GS.getStore('notes');
  },
  deleteNote(id) {
    return this.getNotes().then(notes => {
      return this.saveNotes(notes.splice(id, 1));
    });
  },
  updateNote(id, obj) {
    return this.getNotes().then(notes => {
      notes[id] = Object.assign(notes[id], obj);
      return this.saveNotes(notes);
    });
  },
  saveNotes(arr) {
    return GS.saveStore('notes', arr).then(() => {
      return this.getNotes();
    });
  },
};

export default NoteSettings;
