import React, { Component } from 'react';
import { Button } from 'react-native';

class NavigateButton extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      disabled: false
    };
  }

  render() {
    return (
      <Button
        style={this.props.style}
        color={this.props.style.color}
        title={this.props.title}
        disabled={this.props.disabled}
        onPress={() => { this.props.func(); }}
      />
    );
  }
}

export default NavigateButton;
