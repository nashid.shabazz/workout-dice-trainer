import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';

import NavigateButton from './NavigateHelper.js';
import OSPicker from './OSHelper.js';

class RouteButtons extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    let buttons = [];
    for (let i = 0; i < this.props.buttons.length; i++) {
      let button = this.props.buttons[i];
      buttons.push(
        <NavigateButton key={i} func={button.func} title={button.title} disabled={button.disabled} style={StyleSheet.flatten(styles.button)} />
      );
    }

    return (
      <View style={styles.container}>
        {buttons}
      </View>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  button: {
    width: 25,
    height: 10,
    tintColor: '#000000',
    color: '#1ef451',
    backgroundColor: '#1ef451'
  }
});

export default RouteButtons;
