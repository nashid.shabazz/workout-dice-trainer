import React, { Component } from 'react';
import { ActivityIndicator, Dimensions, Keyboard, Picker, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import TimerEnhance from 'react-native-smart-timer-enhance';
import DatePicker from 'react-native-datepicker';
import Toast from 'react-native-smart-toast';

import OSPicker from './utils/OSHelper.js';
import UserSettings from './utils/UserHelper.js';
import RouteButtons from './utils/RouteHelper.js';
import GlobalSettings from './utils/SettingsHelper.js';

const SPC = ' ';

class EntryInput extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      Fullname: '',
      Nickname: '',
      Gender: 'male',
      Height: '',
      Weight: '',
      Birthdate: OSPicker.mmddyyyyFormatDateYYIncr((new Date()).toString(), -5),
      isInitialized: false
    };
  }

  componentDidMount() {
    setTimeout(() => {
      switch(this.props.id) {
        case 'Fullname':
        case 'Nickname':
          this.props.ufunc(OSPicker.validateText(this.props.state[this.props.id], this.props.id), 'valids', this.props.id);
          break;
        case 'Email':
          this.props.ufunc(OSPicker.validateEmail(this.props.state[this.props.id]), 'valids', this.props.id);
          break;
        default:
          break;
      }
    }, 100);
  }

  componentWillReceiveProps(nextProps) {
    if (!this.state.isInitialized) {
      this.setState({
        Fullname: nextProps.state.Fullname,
        Nickname: nextProps.state.Nickname,
        Gender: nextProps.state.Gender,
        Birthdate: nextProps.state.Birthdate,
        Height: nextProps.state.Height,
        Weight: nextProps.state.Weight,
        isInitialized: true
      });
    }
  }

  simulateDatePress() {
    this.datePicker.onPressDate();
  }

  formatCustom(id, text, parentState, toggleButton, updateFunc) {
    let formattedValue;
    switch (id) {
      case 'Height':
      case 'Weight':
        formattedValue = (id === 'Height') ? OSPicker.formatHeight(text, parentState.isMetric) : OSPicker.formatWeight(text, parentState.isMetric);
        this.onChangeNumber(id, formattedValue, 'numeric', updateFunc);
        break;
      default:
        formattedValue = text;
        break;
    }
    updateFunc(formattedValue, id);
    parentState.isDisabled = !OSPicker.validateForm(parentState.valids);
    toggleButton();
    return formattedValue;
  }

  formatNumber(id, parentState, updateFunc) {
    switch (id) {
      case 'Height':
        const formattedHeight = OSPicker.formatHeight(this.state[id], parentState.isMetric);
        updateFunc(formattedHeight, id);
        this.setState({ Height: formattedHeight });
        break;
      case 'Weight':
        const formattedWeight = OSPicker.formatWeight(this.state[id], parentState.isMetric);
        updateFunc(formattedWeight, id);
        this.setState({ Weight: formattedWeight });
        break;
      default:
        break;
    }
  }

  onChangeForm(type, id, text, parentState, toggleButton, updateForm) {
    this.setState({ [id]: text });

    switch(type) {
      case 'picker':
        this.onChangePicker(id, text, updateForm);
        if (parentState.Target === 'weight') {
          this.formatNumber('TargetValue', parentState, updateFunc);
        }
        break;
      case 'numeric':
        this.onChangeNumber(id, text, type, updateForm);
        break;
      case 'date':
        this.onChangeBirthdate(id, text, updateForm);
        break;
      case 'email-address':
        this.onChangeEmail(id, text, updateForm);
        break;
      case 'text':
      default:
        this.onChangeText(id, text, type, updateForm);
        break;
    }
    parentState.isDisabled = !OSPicker.validateForm(parentState.valids);
    toggleButton();
  }

  onChangePicker(id, text, updateFunc) {
    updateFunc(text, id);
  }

  onChangeEmail(id, email, updateFunc) {
    updateFunc(email, id);
    updateFunc(OSPicker.validateEmail(email), 'valids', id);
  }

  onChangeBirthdate(id, date, updateFunc) {
     this.setState({ Birthdate: date.toString() });
     updateFunc(OSPicker.mmddyyyyFormatDate(date.toString()), id);
     updateFunc(OSPicker.validateDate(new Date(date.toString())), 'valids', id);
   }

  onChangeNumber(id, number, type, updateFunc) {
    updateFunc(((id === 'Height' || id === 'Weight') && parseInt(number) === 0) ? true : OSPicker.validateNumber(number, type), 'valids', id);
  }

  onChangeText(id, text, type, updateFunc) {
    updateFunc(text, id);
    updateFunc(OSPicker.validateText(text, id), 'valids', id);
  }

  onFocus(id, updateFunc) {
    updateFunc(id, 'activeView');
  }

  toggleType(type, id, choices, parentState, updateFunc) {
    if (type === 'picker') {
      updateFunc(OSPicker.nextQueueValue(Object.values(choices).map(c => OSPicker.trim(c, true).toLowerCase()), OSPicker.trim(parentState[id], true)), id);
      parentState.isDisabled = parentState.isDisabled = !OSPicker.validateForm(parentState.valids);
    }
  }

  render() {
    const validationTextColor = this.props.state.valids[this.props.id] ? 'green' : 'red';
    const validationTitleColor = this.props.state.valids[this.props.id] ? 'black' : 'red';
    let entryInput;
    switch (this.props.type) {
      case 'date':
        entryInput = (
          <View>
            <DatePicker
              ref={(picker) => { this.datePicker = picker; }}
              style={OSPicker.isPlatformIOS() ? {width: 0, height: 0} : {width: 275, display: 'none'}}
              date={this.state[this.props.id]}
              mode="date"
              placeholder="select date"
              format="MM/DD/YYYY"
              minDate="01/01/1900"
              maxDate={OSPicker.mmddyyyyFormatDateYYIncr((new Date()).toString(), -5)}
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              customStyles={{
                dateIcon: {
                  position: 'absolute',
                  left: 0,
                  top: 4,
                  marginLeft: 0
                },
                dateInput: {
                  marginLeft: 36
                }
              }}
              onDateChange={(date) => { this.onChangeForm(this.props.type, this.props.id, date, this.props.state, this.props.func, this.props.ufunc); }}
            />
            <TouchableOpacity onPress={() => { this.simulateDatePress(); }}>
              <View pointerEvents='none'>
                <TextInput
                  style={{fontSize: 20, width: 225, height: 45, textAlign: 'center', color: validationTextColor }}
                  keyboardType={this.props.keyBoardType}
                  value={OSPicker.mmddyyyyFormatDate(this.state[this.props.id])}
                />
              </View>
            </TouchableOpacity>
          </View>
        );
        break;
      case 'email-address':
        entryInput = (
          <TextInput
            style={{fontSize: 20, width: 275, height: 45, textAlign: 'center', color: validationTextColor }}
            autoCapitalize={this.props.autoCapitalize ? this.props.autoCapitalize : 'none'}
            keyboardType={this.props.type}
            maxLength={100}
            placeholder={this.props.placeholder}
            value={this.props.state[this.props.id]}
            onFocus={() => { this.onFocus(this.props.id, this.props.ufunc); }}
            onChangeText={(email) => { this.onChangeForm(this.props.type, this.props.id, email, this.props.state, this.props.func, this.props.ufunc); }}
          />
        );
        break;
      case 'picker':
        const keys = Object.keys(this.props.choices);
        let items = [];
        for (let k in keys) {
          const c = keys[k];
          items = items.concat([ <Picker.Item key={this.props.choices[c]} label={this.props.choices[c]} color={validationTextColor} value={c} /> ]);
        }
        entryInput = (
          <View style={{paddingBottom: OSPicker.isPlatformDroid ? 10 : 0}}>
            <View style={OSPicker.isPlatformDroid ? {borderBottomColor: 'black', borderBottomWidth: 1} : {}}>
                <Picker
                  style={OSPicker.isPlatformDroid ? {alignItems: 'center'} : {}, {width: (this.props.id === 'Gender') ? 125 : 175}}
                  itemStyle={OSPicker.isPlatformIOS ? {color: validationTextColor, height: 90} : {}}
                  selectedValue={this.props.state[this.props.id]}
                  onValueChange={(val) => { this.onChangeForm(this.props.type, this.props.id, val, this.props.state, this.props.func, this.props.ufunc); }}>
                  {items}
                </Picker>
            </View>
          </View>
        );
        break;
      case 'numeric':
        entryInput = (
          <TextInput
            style={{fontSize: 20, width: 200, height: 45, textAlign: 'center', color: validationTextColor }}
            keyboardType={this.props.type}
            selectTextOnFocus={true}
            maxLength={this.props.id === 'Height' ? 6 : (this.props.id === 'Weight' ? 8 : 3)}
            value={this.props.state[this.props.id].toString()}
            onFocus={() => { this.onFocus(this.props.id, this.props.ufunc); }}
            onChangeText={(text) => { this.setState({ [this.props.id]: this.formatCustom(this.props.id, text, this.props.state, this.props.func, this.props.ufunc) }); }}
          />
        );
        break;
      default:
        entryInput = (
          <TextInput
            style={{fontSize: 20, width: 275, height: 45, textAlign: 'center', color: validationTextColor }}
            autoCapitalize={this.props.autoCapitalize ? this.props.autoCapitalize : 'none'}
            keyboardType={this.props.type}
            maxLength={100}
            placeholder={this.props.placeholder}
            value={this.props.state[this.props.id]}
            onFocus={() => { this.onFocus(this.props.id, this.props.ufunc); }}
            onChangeText={(text) => { this.onChangeForm(this.props.type, this.props.id, text, this.props.state, this.props.func, this.props.ufunc); }}
          />
        );
        break;
    }
    return (
      <View style={(this.props.state.inProgress || (this.props.state.keyboardSpace && this.props.id !== this.props.state.activeView)) ? {display: 'none', marginBottom: 0} : ((this.props.state.keyboardSpace && this.props.id === this.props.state.activeView) ? Object.assign({position: 'absolute'}, this.props.state.deviceOrientation === 'portrait' ? {bottom: this.props.state.keyboardSpace} : {top: 10}) : {marginBottom: 10})}>
        {entryInput}
        <TouchableOpacity onPress={() => { this.toggleType(this.props.type, this.props.id, this.props.choices, this.props.state, this.props.ufunc); }}>
          <Text style={{ fontSize: 15, textAlign: 'center', color: validationTitleColor, lineHeight: 15, transform: [{scaleX: 1.5}] }}>
            {OSPicker.splitProperNouns(this.props.id)}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

class Portal extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isScrollInitialized: false
    };
  }

  componentDidMount() {
    setTimeout(() => {
      if (!this.state.isScrollInitialized) {
        this.props.onupdate(false, 'isScroll');
      }
    }, 5000);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.state.deviceOrientation != this.props.state.deviceOrientation) {
      nextProps.onupdate(true, 'isScroll');
    }
  }

  onScroll = (e, parentState, updateFunc) => {
    if (!this.state.isScrollInitialized) {
      this.setState({
        isScrollInitialized: true
      });
    }
    if (parentState.isScroll === true) {
      const height = e.nativeEvent.contentSize.height + parentState.footerHeight,
          offset = e.nativeEvent.contentOffset.y;
      if (parseInt(offset + height) >= parseInt(parentState.screenHeight)) {
          updateFunc(false, 'isScroll');
      }
    }
  }

  render() {
    content = ( <ScrollView contentContainerStyle={styles.container}><Text>{'Content failed to load'}</Text></ScrollView> );
    if (true) {
      content = (
        <ScrollView contentContainerStyle={styles.container} onScroll={(event) => { this.onScroll(event, this.props.state, this.props.onupdate); }}>
          <EntryInput id='Fullname' type={'default'} state={this.props.state} autoCapitalize={'words'} func={this.props.onchange} ufunc={this.props.onupdate} placeholder={'First Last'} />
          <EntryInput id='Nickname' type={'default'} state={this.props.state} func={this.props.onchange} ufunc={this.props.onupdate} placeholder={'Alias'} />
          <EntryInput id='Birthdate' type={'date'} state={this.props.state} func={this.props.onchange} ufunc={this.props.onupdate} />
          <EntryInput id='Gender' type={'picker'} state={this.props.state} choices={{male: 'Male', female: 'Female'}} func={this.props.onchange} ufunc={this.props.onupdate} />
          <EntryInput id='Weight' type={'numeric'} state={this.props.state} func={this.props.onchange} ufunc={this.props.onupdate} />
          <EntryInput id='Height' type={'numeric'} state={this.props.state} func={this.props.onchange} ufunc={this.props.onupdate} />
          <EntryInput id='Email' type={'email-address'} state={this.props.state} func={this.props.onchange} ufunc={this.props.onupdate} placeholder={'user@domain.org'} />
          <ActivityIndicator size="large" color={styles.indicator.color} style={!this.props.state.inProgress ? {display: 'none'} : {}} />
        </ScrollView>
      );
    }
    return content;
  }
}

class RegisterScreen extends React.Component {
  static navigationOptions = {
      title: 'BazzTech™️ - Dice-Fit Trainer'
  };

  constructor(props) {
    super(props);
    this.state = {
      valids: { Birthdate: true, Gender: true, Fullname: false, Email: false, Height: true, Nickname: false, Weight: true },
      showComponent: true,
      inProgress: false,
      isDisabled: true,
      isMetric: false,
      isScroll: true,
      Email: '',
      Nickname: '',
      Fullname: '',
      Gender: 'male',
      Height: '5\'6\"',
      Weight: '163 lbs',
      activeView: '',
      Birthdate: OSPicker.mmddyyyyFormatDateYYIncr((new Date()).toString(), -5),
      keyboardSpace: 0,
      screenWidth: 0,
      screenHeight: 0,
      footerHeight: 30,
      deviceOrientation: OSPicker.getDeviceOrientation(),
    };
    this.indicator = null;
    this.toggleRouteButton = this.toggleRouteButton.bind(this);
    this.updateFormFields = this.updateFormFields.bind(this);
    Dimensions.addEventListener('change', () => {
      this.setState({
        screenWidth: Dimensions.get('window').width,
        screenHeight: Dimensions.get('window').height,
        deviceOrientation: OSPicker.getDeviceOrientation()
      }, () => {
        this.clearIndicator();
        if (this.state.keyboardSpace) {
          Keyboard.dismiss();
        }
      });
    });
    Keyboard.addListener('keyboardDidShow', (frames) => {
      if (!frames.endCoordinates) return;
      this.setState({keyboardSpace: frames.endCoordinates.height});
    });
    Keyboard.addListener('keyboardDidHide', (frames) => {
      this.setState({keyboardSpace: 0, activeView: '' });
    });
  }

  componentDidMount() {
    GlobalSettings.isMetricSystem().then(isMetricSystem => {
      UserSettings.getUser().then((user) => {
        if (user !== null) {
          this.setState({
            Fullname: user.name,
            Nickname: user.nick,
            Gender: user.gender,
            Height: user.height,
            Weight: user.weight,
            Email: user.email,
            Birthdate: user.dob,
            System: isMetricSystem ? 'metric' : 'imperial'
          });
        }
      });
    });
  }

  componentWillMount() {
    this.setState({
      screenWidth: Dimensions.get('window').width,
      screenHeight: Dimensions.get('window').height,
      deviceOrientation: OSPicker.getDeviceOrientation()
    });
  }

  componentWillUnmount() {
    this.clearIndicator();
  }

  clearIndicator() {
    if (this.indicator != null) {
      this.hideToast();
      clearInterval(this.indicator);
    }
  }

  toggleRouteButton() {
    this.setState(state => ({
      disabled: state.isDisabled
    }));
  }

  updateFormFields(value, id1, id2 = null) {
    if (id2 === null) {
      this.setState({
        [id1]: value
      });
    } else {
      let obj = this.state[id1];
      obj[id2] = value;
      this.setState({
        [id1]: obj
      });
    }
  }

  showToast() {
    if (this.toast) {
      this.toast.show({
        duration: 1000,
        position: Toast.constants.gravity.bottom,
        children: <View><Text style={{color: '#008000'}}>{'▼'}</Text></View>,
        animationEnd: () => { this.hideToast(); }
      });
    }
  }

  hideToast() {
    if (this.toast) {
      this.toast.hide({ duration: 400 });
    }
  }

  onLayout() {
    this.showToast();
    this.indicator = setInterval(() => {
      if (this.state.isScroll) {
        this.showToast();
      } else {
        this.clearIndicator();
      }
    }, 2000);
  }

  registerUser(goToScreen) {
    const msg = 'By registering, you agree to receive promotional emails from' + SPC +
              'BazzTech™️ and its associates. All registration information sent to BazzTech™️' + SPC +
              'servers is secure and private and will not be shared with third parties' + SPC +
              'unless given expressed written consent.\n\nYou may opt out of these emails' + SPC +
              'by replying to the sender indicating to stop.';
    OSPicker.showConfirmDialog('BazzTech™️ Registration', msg, ['Disagree', 'Agree'], true, () => { return; }, () => {
      const toastMessage = "Registration successful!";
      this.clearIndicator();
      if (!this.state.isDisabled) {
        this.setState({ inProgress: true, isDisabled: true }, () => {
          UserSettings.addUser(this.state).then(userData => {
            GlobalSettings.initSettings({ id: userData.id }).then((settings) => {
              userData.settings = settings;
              UserSettings.registerUser(userData).then((toastMessage) => {
                this.hideView();
                goToScreen('LoadScreen', {screen: 'HomeScreen', user: userData, toast: toastMessage});
                this.setState({ inProgress: false, isDisabled: false });
              });
            });
          }).catch((error) => {
            console.error('registerUser', error);
          });
        });
      }
    });
  }

  hideView() {
    this.setState({ showComponent: false });
  }

  render() {
    const { state, navigate } = this.props.navigation;
    const rbuttons = [
        { title: 'Register User', func: () => this.registerUser(navigate), disabled: this.state.isDisabled || this.state.inProgress },
    ];
    if (!this.state.showComponent) {
      return null;
    } else {
      return (
        <View style={{flex: 1, flexDirection: 'column', justifyContent: 'space-between'}} onLayout={() => { this.onLayout(); }}>
          <Portal state={this.state} onchange={this.toggleRouteButton} onupdate={this.updateFormFields}/>
          <Toast ref={(toast) => { this.toast = toast; }} spacing={100} leftX={this.state.screenWidth * 0.95} style={{backgroundColor: 'transparent'}}></Toast>
          <RouteButtons buttons={rbuttons} />
        </View>
      );
    }
  }
};

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    flexDirection: 'column',
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  indicator: {
    color: '#00ff00'
  }
});

export default TimerEnhance(RegisterScreen);
