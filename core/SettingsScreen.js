import React, { Component } from 'react';
import { Dimensions, Image, Keyboard, Picker, ScrollView, StyleSheet, Switch, Text, TextInput, TouchableOpacity, View } from 'react-native';
import TimerEnhance from 'react-native-smart-timer-enhance';
import Toast from 'react-native-smart-toast';

import OSPicker from './utils/OSHelper.js';
import UserSettings from './utils/UserHelper.js';
import RouteButtons from './utils/RouteHelper.js';
import GlobalSettings from './utils/SettingsHelper.js';

class EntryInput extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      Nickname: '',
      Gender: '',
      Height: '',
      Weight: '',
      Units: 'imperial',
      isInitialized: false,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (!this.state.isInitialized) {
      this.setState({
        Nickname: nextProps.state.Nickname,
        Gender: nextProps.state.Gender,
        Height: nextProps.state.Height,
        Weight: nextProps.state.Weight,
        Units: nextProps.state.Units,
        isInitialized: true
      });
    }
  }

  formatCustom(id, text, parentState, toggleButton, updateFunc, isMetric = null) {
    let formattedValue;
    switch (id) {
      case 'Height':
      case 'Weight':
        formattedValue = (id === 'Height') ? OSPicker.formatHeight(text, !OSPicker.isUndefinedOrNull(isMetric) ? isMetric : parentState.isMetric) : OSPicker.formatWeight(text, !OSPicker.isUndefinedOrNull(isMetric) ? isMetric : parentState.isMetric);
        this.onChangeNumber(id, formattedValue, 'numeric', updateFunc);
        break;
      default:
        formattedValue = text;
        break;
    }
    updateFunc(formattedValue, id);
    parentState.isDisabled = !OSPicker.validateForm(parentState.valids);
    toggleButton();

    return formattedValue;
  }

  formatNumber(id, parentState, updateFunc, isMetric = null) {
    const updateFormatted = function(formattedType, formattedValue) {
      updateFunc(formattedValue, id);
    };
    const isMetricType = !OSPicker.isUndefinedOrNull(isMetric) ? isMetric : parentState.isMetric;
    switch (id) {
      case 'Height':
        updateFormatted('Height', OSPicker.formatHeight(OSPicker.convertHeight(parentState.Height, isMetricType), isMetricType));
        break;
      case 'Weight':
        updateFormatted('Weight', OSPicker.formatWeight(OSPicker.convertWeight(parentState.Weight, isMetricType), isMetricType));
        break;
      default:
        break;
    }
  }

  onChangeMetric(id, text, parentState, updateFunc) {
    if (id === 'Units' && parentState.Units !== text) {
      const isMetric = text === 'metric';
      this.formatNumber('Height', parentState, updateFunc, isMetric);
      this.formatNumber('Weight', parentState, updateFunc, isMetric);
    }
  }

  onChangeForm(type, id, text, parentState, toggleButton, updateForm) {
    this.setState({ [id]: text });

    switch(type) {
      case 'picker':
        this.onChangePicker(id, text, updateForm);
        this.onChangeMetric(id, text, parentState, updateForm);
        break;
      case 'numeric':
        this.onChangeNumber(id, text, type, updateForm);
        break;
      case 'text':
      default:
        this.onChangeText(id, text, type, updateForm);
        break;
    }
    parentState.isDisabled = !OSPicker.validateForm(parentState.valids);
    toggleButton();
  }

  onChangePicker(id, text, updateFunc) {
    updateFunc(text, id);
  }

  onChangeEmail(id, email, updateFunc) {
    updateFunc(email, id);
    updateFunc(OSPicker.validateEmail(email), 'valids', id);
  }

  onChangeNumber(id, number, type, updateFunc) {
    updateFunc(((id === 'Height' || id === 'Weight') && parseInt(number) === 0) ? true : OSPicker.validateNumber(number, type), 'valids', id);
  }

  onChangeText(id, text, type, updateFunc) {
    updateFunc(text, id);
    updateFunc(OSPicker.validateText(text, id), 'valids', id);
  }

  onFocus(id, updateFunc) {
    updateFunc(id, 'activeView');
  }

  toggleType(type, id, choices, parentState, updateFunc) {
    if (type === 'picker') {
      updateFunc(OSPicker.nextQueueValue(Object.values(choices).map(c => OSPicker.trim(c, true).toLowerCase()), OSPicker.trim(parentState[id], true)), id);
      this.onChangeMetric(id, parentState.Units === 'metric' ? 'imperial' : 'metric', parentState, updateFunc);
      parentState.isDisabled = !OSPicker.validateForm(parentState.valids);
    }
  }

  render() {
    const validationTextColor = this.props.state.valids[this.props.id] ? 'green' : 'red';
    const validationTitleColor = this.props.state.valids[this.props.id] ? 'black' : 'red';
    let entryInput;
    switch (this.props.type) {
      case 'picker':
        const keys = Object.keys(this.props.choices);
        let items = [];
        for (let k in keys) {
          const c = keys[k];
          items = items.concat([ <Picker.Item key={this.props.choices[c]} label={this.props.choices[c]} color={validationTextColor} value={c} /> ]);
        }
        entryInput = (
          <View style={{paddingBottom: OSPicker.isPlatformDroid ? 10 : 0}}>
            <View style={OSPicker.isPlatformDroid ? {borderBottomColor: 'black', borderBottomWidth: 1} : {}}>
              <Picker
                style={OSPicker.isPlatformDroid ? {alignItems: 'center'} : {}, {width: 125}}
                itemStyle={OSPicker.isPlatformIOS ? {color: validationTextColor, height: 90} : {}}
                selectedValue={this.props.state[this.props.id]}
                onValueChange={(val) => { this.onChangeForm(this.props.type, this.props.id, val, this.props.state, this.props.func, this.props.ufunc); }}>
                {items}
              </Picker>
            </View>
          </View>
        );
        break;
      case 'numeric':
        entryInput = (
          <TextInput
            style={{fontSize: 20, width: 200, height: 45, textAlign: 'center', color: validationTextColor }}
            keyboardType={this.props.type}
            selectTextOnFocus={true}
            maxLength={this.props.id === 'Height' ? 6 : (this.props.id === 'Weight' ? 8 : 3)}
            value={this.props.state[this.props.id].toString()}
            onFocus={() => { this.onFocus(this.props.id, this.props.ufunc); }}
            onChangeText={(text) => { this.setState({ [this.props.id]: this.formatCustom(this.props.id, text, this.props.state, this.props.func, this.props.ufunc) }); }}
          />
        );
        break;
      default:
        entryInput = (
          <TextInput
            style={{fontSize: 20, width: 275, height: 45, textAlign: 'center', color: validationTextColor }}
            autoCapitalize={this.props.autoCapitalize ? this.props.autoCapitalize : 'none'}
            keyboardType={this.props.type}
            maxLength={100}
            placeholder={this.props.placeholder}
            value={this.props.state[this.props.id]}
            onFocus={() => { this.onFocus(this.props.id, this.props.ufunc); }}
            onChangeText={(text) => { this.onChangeForm(this.props.type, this.props.id, text, this.props.state, this.props.func, this.props.ufunc); }}
          />
        );
        break;
    }
    return (
      <View style={(this.props.state.keyboardSpace && this.props.id !== this.props.state.activeView) ? {display: 'none', marginBottom: 0} : ((this.props.state.keyboardSpace && this.props.id === this.props.state.activeView) ? Object.assign({position: 'absolute'}, this.props.state.deviceOrientation === 'portrait' ? {bottom: this.props.state.keyboardSpace} : {top: 10}) : {marginBottom: 10})}>
        {entryInput}
        <TouchableOpacity onPress={() => { this.toggleType(this.props.type, this.props.id, this.props.choices, this.props.state, this.props.ufunc); }}>
          <Text style={{ fontSize: 15, textAlign: 'center', color: validationTitleColor, lineHeight: 15, transform: [{scaleX: 1.5}] }}>
            {OSPicker.splitProperNouns(this.props.id)}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

class Portal extends React.Component {

  componentWillReceiveProps(nextProps) {
    if (nextProps.state.deviceOrientation != this.props.state.deviceOrientation) {
      nextProps.onupdate(nextProps.state.deviceOrientation !== 'portrait', 'isScroll');
    }
  }

  onScroll = (e, parentState, updateFunc) => {
    if (parentState.isScroll === true) {
      const height = e.nativeEvent.contentSize.height + parentState.footerHeight,
          offset = e.nativeEvent.contentOffset.y;
      if (parseInt(offset + height) >= parseInt(parentState.screenHeight)) {
          updateFunc(false, 'isScroll');
      }
    }
  }

  render() {
    content = ( <ScrollView contentContainerStyle={styles.container}><Text>{'Content failed to load'}</Text></ScrollView> );
    if (true) {
      content = (
        <ScrollView contentContainerStyle={styles.container} onScroll={(event) => { this.onScroll(event, this.props.state, this.props.onupdate); }}>
          <EntryInput id='Nickname' type={'default'} state={this.props.state} func={this.props.onchange} ufunc={this.props.onupdate} placeholder={'Alias'} />
          <EntryInput id='Gender' type={'picker'} state={this.props.state} choices={{male: 'Male', female: 'Female'}} func={this.props.onchange} ufunc={this.props.onupdate} />
          <EntryInput id='Weight' type={'numeric'} state={this.props.state} func={this.props.onchange} ufunc={this.props.onupdate} />
          <EntryInput id='Height' type={'numeric'} state={this.props.state} func={this.props.onchange} ufunc={this.props.onupdate} />
          <EntryInput id='Units' type={'picker'} state={this.props.state} choices={{imperial: 'Imperial', metric: 'Metric'}} func={this.props.onchange} ufunc={this.props.onupdate} />
          <View pointerEvents='box-none' style={{position: 'absolute', left: 10, bottom: 10, justifyContent: 'center', alignItems: 'center', zIndex: OSPicker.getMaxInteger() / 2}}>
            <Text adjustsFontSizeToFit={true} numberOfLines={1} style={{color: '#000000', fontSize: 16}}>{OSPicker.getVersion()}</Text>
          </View>
        </ScrollView>
      );
    }
    return content;
  }
}

class SettingsScreen extends React.Component {
  static navigationOptions = {
      title: 'BazzTech™️ - Dice-Fit Trainer'
  };

  constructor(props) {
      super(props);
      this.state = {
        valids: { Gender: true, Nickname: true, Height: true, Weight: true, Units: true },
        showComponent: true,
        isDisabled: true,
        isMetric: false,
        isScroll: false,
        Nickname: 'nickname',
        Gender: 'male',
        Height: '5\'6\"',
        Weight: '163 lbs',
        Units: 'imperial',
        activeView: '',
        keyboardSpace: 0,
        screenWidth: 0,
        screenHeight: 0,
        toastPosition: 0,
        footerHeight: 30,
        deviceOrientation: OSPicker.getDeviceOrientation(),
      };
      this.indicator = null;
      this.toggleRouteButton = this.toggleRouteButton.bind(this);
      this.updateFormFields = this.updateFormFields.bind(this);
      Dimensions.addEventListener('change', () => {
        this.setState({
          screenWidth: Dimensions.get('window').width,
          screenHeight: Dimensions.get('window').height,
          deviceOrientation: OSPicker.getDeviceOrientation()
        }, () => {
          if (this.state.keyboardSpace) {
            Keyboard.dismiss();
          }
        });
      });
      Keyboard.addListener('keyboardDidShow', (frames) => {
        if (!frames.endCoordinates) return;
        this.setState({keyboardSpace: frames.endCoordinates.height});
      });
      Keyboard.addListener('keyboardDidHide', (frames) => {
        this.setState({keyboardSpace: 0, activeView: '' });
      });
  }

  componentDidMount() {
    const params = this.props.navigation.state.params;
    UserSettings.getUser((params && params.user) ? params.user : null).then(user => {
      if (!OSPicker.isUndefinedOrNull(user)) {
        GlobalSettings.isMetricSystem().then((isMetricSystem) => {
          this.setState({
            isMetric: isMetricSystem,
            Nickname: user.nick,
            Gender: user.gender,
            Height: OSPicker.convertHeight(user.height, isMetricSystem),
            Weight: OSPicker.convertWeight(user.weight, isMetricSystem),
            Units: isMetricSystem ? 'metric' : 'imperial'
          });
        });
      }
    }).catch(error => {
      console.error(error.message);
    });
  }

  componentWillMount() {
    this.setState({
      screenWidth: Dimensions.get('window').width,
      screenHeight: Dimensions.get('window').height,
      deviceOrientation: OSPicker.getDeviceOrientation()
    });
  }

  componentWillUnmount() {
    this.clearIndicator();
  }

  clearIndicator() {
    if (this.indicator != null) {
      this.hideToast();
      clearInterval(this.indicator);
    }
  }

  toggleRouteButton() {
    this.setState(state => ({
      disabled: state.isDisabled
    }));
  }

  isDefaultState() {
    return this.state.Nickname === 'nickname' && this.state.Gender === 'male' && this.state.Height === '5\'6"' && this.state.Weight === '163 lbs' && this.state.Units === 'imperial';
  }

  updateFormFields(value, id1, id2 = null) {
    let obj = this.state[id1];
    if (id2 === null) {
      obj = value;
      this.setState({
        [id1]: value
      });
      // Handle custom validation
      let vobj = this.state.valids;
      if (id1 === 'Target') {
        vobj.TargetValue = (value === 'weight') ? true : this.state.TargetValue !== '0';
      }
      this.setState({
        valids: vobj
      });
    } else {
      obj[id2] = value;
      this.setState({
        [id1]: obj
      });
    }
  }

  updateSettings(goToScreen, defaults = false) {
    OSPicker.showConfirmDialog(defaults ? 'Restore Defaults' : 'Update Settings', 'Are you sure?', null, true, () => {
      let userUpdates = {
        nick: this.state.Nickname,
        gender: this.state.Gender,
        height: this.state.Height,
        weight: this.state.Weight
      };
      let settingsUpdates = {
        units: this.state.Units
      };
      let toastMessage = 'User settings updated!';
      if (defaults) {
        // Restore Default Settings
        toastMessage = 'Default settings restored!';
        settingsUpdates.units = 'imperial';
        userUpdates = {
          nick: 'nickname',
          gender: 'male',
          height: '5\'6\"',
          weight: '163 lbs'
        };
      }
      UserSettings.updateUser(userUpdates).then(userData => {
        GlobalSettings.updateSettings(settingsUpdates).then((settings) => {
          userData.settings = settings;
          if (defaults) {
            this.setState({
              valids: { Gender: true, Nickname: true, Height: true, Weight: true, Units: true },
              isDisabled: true,
              isMetric: false,
              isChanged: false,
              Nickname: userUpdates.nick,
              Gender: userUpdates.gender,
              Height: userUpdates.height,
              Weight: userUpdates.weight,
              Units: settingsUpdates.units
            });
            this.messageToast.show({
              position: Toast.constants.gravity.bottom,
              children: <View><Text style={{color: '#ffffff'}}>{toastMessage}</Text></View>
            });
          } else {
            this.hideView();
            goToScreen('LoadScreen', {screen: 'HomeScreen', user: userData, toast: toastMessage});
          }
        });
      }).catch(error => {
        console.error(error.message);
      });
    });
  }

  showToast() {
    if (this.toast) {
      this.toast.show({
        duration: 1000,
        position: Toast.constants.gravity.bottom,
        children: <View><Text style={{color: '#008000'}}>{'▼'}</Text></View>,
        animationEnd: () => { this.hideToast(); }
      });
    }
  }

  hideToast() {
    if (this.toast) {
      this.toast.hide({ duration: 400 });
    }
  }

  onLayout() {
    this.showToast();
    this.indicator = setInterval(() => {
      if (this.state.isScroll) {
        this.showToast();
      } else {
        this.clearIndicator();
      }
    }, 2000);
  }

  hideView() {
    this.setState({ showComponent: false });
  }

  render() {
    const { state, navigate } = this.props.navigation;
    const isPortraitMode = this.state.deviceOrientation === 'portrait';
    let discardTitle = isPortraitMode ? 'Discard' : 'Discard Changes';
    let updateTitle = isPortraitMode ? 'Update Settings' : 'Update User Settings';
    let restoreTitle = isPortraitMode  ? 'Restore' : 'Restore Defaults';
    if ((isPortraitMode || this.state.screenWidth <= 480) && OSPicker.getDeviceAspectRatio() <= 1.5) {
      discardTitle = 'Discard';
      updateTitle = isPortraitMode ? 'Update' : 'Update Settings';
      restoreTitle = 'Restore';
    }
    const rbuttons = [
      { title: discardTitle, func: () => { this.hideView(); navigate("LoadScreen", {screen: "HomeScreen", user: state.params.user}); }, disabled: false },
      { title: updateTitle, func: () => this.updateSettings(navigate, false), disabled: this.state.isDisabled },
      { title: restoreTitle, func: () => this.updateSettings(navigate, true), disabled: this.isDefaultState() },
    ];
    if (!this.state.showComponent) {
      return null;
    } else {
      return (
        <View style={{flex: 1, flexDirection: 'column', justifyContent: 'space-between'}} onLayout={() => { this.onLayout(); }}>
          <Portal state={this.state} onchange={this.toggleRouteButton} onupdate={this.updateFormFields} />
          <Toast ref={(toast) => { this.messageToast = toast; }} style={{marginBottom: 64, backgroundColor: 'rgba(0,128,0,0.4)'}}></Toast>
          <Toast ref={(toast) => { this.toast = toast; }} spacing={100} leftX={this.state.screenWidth * 0.95} style={{backgroundColor: 'transparent'}}></Toast>
          <RouteButtons buttons={rbuttons} />
        </View>
      );
    }
  }
};

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    flexDirection: 'column',
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  }
});

export default TimerEnhance(SettingsScreen);
