import React, { Component } from 'react';
import { ActivityIndicator, Alert, Dimensions, Image, Keyboard, PureComponent, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { SwipeableFlatList } from 'react-native-swipeable-flat-list';
import * as Animatable from 'react-native-animatable';

import CheckBox from './ui/CheckBoxImage.js';
import Indicator from './ui/CustomIndicator.js';

import OSPicker from './utils/OSHelper.js';
import UserSettings from './utils/UserHelper.js';
import GoalSettings from './utils/GoalHelper.js';
import RouteButtons from './utils/RouteHelper.js';
import WorkoutSettings from './utils/WorkoutHelper.js';
import GlobalSettings from './utils/SettingsHelper.js';

class ViewHeader extends React.Component {

  constructor(props) {
    super(props);
  }

  showHelpDialog(index, type) {
    let message, extra = '';
    switch (OSPicker.trim(type.toLowerCase(), true)) {
      case 'fit':
      case 'slim':
      case 'muscular':
        message = '\"' + type.toUpperCase() + '\" is the Target Profile that determines specific workout characteristics';
        break;
      case 'weeks':
      case 'months':
        extra = ' spread over a series of ' + type.toLowerCase();
      case 'rhr':
      case 'weight':
        extra = ' repeated until the Target Value is reached';
      case 'custom':
      case 'duration':
      case 'bodybuild':
        message = '\"' + type.toUpperCase() + '\" indicates the Target Workout Type' + extra;
        break;
      case 'combo':
        extra = ' includes both HOME and GYM dice, each';
      case 'home':
      case 'gym':
        message = '\"' + type.toUpperCase() + '\" dice' + extra + ' rolled with REPEATER dice to determine workout sets and repetitions (REPS)';
        break;
      case (index !== 3 ? 'average' : ''):
      case (index !== 3 ? 'toned' : ''):
      case (index !== 3 ? 'thin' : ''):
        message = '\"' + type.toUpperCase() + '\" is 1 of 3 Target Body Builds (TBB) which indicate expected body physique after completing the entire workout';
        break;
      case 'beginner':
      case 'trainee':
      case 'expert':
        message = '\"' + type.toUpperCase() + '\" indicates the Target Workout Difficulty';
        break;
      default:
        message = '\"Target Value\" specifies the achievement constant (based on Duration Type) to complete a workout profile';
        break;
    }
    OSPicker.showHelpDialog(message);
  }

  render() {
    let headerItems = [];
    const textTransformStyle = Object.assign(this.props.state.deviceOrientation === 'portrait' && OSPicker.getDeviceAspectRatio() <= 1.5 ? {fontSize: 9} : {}, {transform: [{scaleX: 0.9}, {scaleY: 2.25}]});
    const optionDataLength = this.props.state.optionData ? this.props.state.optionData.length : 0;
    for (let i = 0; i < optionDataLength; i++) {
      headerItems = headerItems.concat([
        <View key={i} style={{backgroundColor: 'white', marginTop: 5, marginBottom: 5, justifyContent: 'center', alignItems: 'stretch'}}>
          <TouchableOpacity style={{height: 40, justifyContent: 'center', borderColor: 'black', borderRadius: 5, borderWidth: 2, alignSelf: 'stretch'}} onLongPress={() => { this.showHelpDialog(i, this.props.state.optionData[i]); }}>
            <Text adjustsFontSizeToFit={true} style={textTransformStyle}>{this.props.state.optionData[i]}</Text>
          </TouchableOpacity>
        </View>
      ]);
    }

    return (
      <View style={{flexShrink: 1, flexDirection: 'row', minHeight: 50, justifyContent: 'space-evenly', borderBottomColor: '#008000', borderBottomWidth: 0.5}}>
        {headerItems}
      </View>
    );
  }
}

class MyListItem extends React.PureComponent {
  render() {
    const onPressFunc = () => { this.props.selectItem(this.props.item, this.props.index, this.props.state); };
    const onLongPressFunc = () => { this.props.viewItem(this.props.item, this.props.index); }
    return (
      <TouchableOpacity style={{height: this.props.style.height, justifyContent: 'center', backgroundColor: this.props.backgroundColor}} onPress={onPressFunc} onLongPress={onLongPressFunc}>
        <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
          <CheckBox style={{flex: 0.14}} func={() => {}} checked={this.props.item.done} disabled={this.props.disabled} style={StyleSheet.flatten({width: 31, height: 31, resizeMode: Image.resizeMode.contain})} />
          <Text adjustsFontSizeToFit={true} style={{flex: 0.14, textAlign: 'center', textAlignVertical: 'center', color: this.props.textColor}}>{this.props.weekText}</Text>
          <Text adjustsFontSizeToFit={true} style={{flex: 0.14, textAlign: 'center', textAlignVertical: 'center', color: this.props.textColor}}>{this.props.dayText}</Text>
          <Text adjustsFontSizeToFit={true} style={{flex: 0.07, textAlign: 'center', textAlignVertical: 'center', color: this.props.textColor}}>{'→'}</Text>
          <Text adjustsFontSizeToFit={true} style={{flex: 0.14, textAlign: 'center', textAlignVertical: 'center', color: this.props.textColor}}>{'Level ' + OSPicker.numberToRomanNumeral(this.props.item.level)}</Text>
          <Text adjustsFontSizeToFit={true} style={{flex: 0.14, textAlign: 'center', textAlignVertical: 'center', color: this.props.textColor}}>{this.props.item.reps + 'x'}</Text>
          <TouchableOpacity style={{flex: 0.14}} onLongPress={() => { this.props.renderHelpDialog(this.props.item.dice); }}>
            <Image style={{width: 31, height: 31, marginRight: 5, resizeMode: Image.resizeMode.contain}} source={{ uri: OSPicker.getImgData().media[this.props.item.dice + '-dice'] }} />
          </TouchableOpacity>
        </View>
      </TouchableOpacity>
    );
  }
}

class Portal extends React.PureComponent {

  constructor(props) {
      super(props);
      this.state = {
        justUpdated: false,
        selectedItem: 0,
        workoutPage: 0,
        workoutFeed: [],
        workoutItems: [],
      };
      this.separatorHeight = 1;
      this.itemHeight =  55 + this.separatorHeight;
      this.dataFeedPage = 0;
      this.pullDownPadding = 128;
      this.dataFeedLength = parseInt((this.props.state.screenHeight - this.pullDownPadding - (this.props.state.footerHeight * 2)) / this.itemHeight) + 1;
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      selectedItem: nextProps.state.selectedWorkout % this.dataFeedLength,
      workoutPage: this.getWorkoutPage(nextProps.state.selectedWorkout)
    });
  }

  getWorkoutPage(index) {
    return parseInt(index / this.dataFeedLength);
  }

  renderHelpDialog(type) {
    const message = '\"' + type.toUpperCase() + '\" dice' + ' may be used inside a ' + type.toLowerCase() + ' and include workouts with' + (type.toLowerCase() === 'gym' ? '' : 'out') + ' weights';
    OSPicker.showHelpDialog(message);
  }

  renderWorkouts = (item, itemIndex, initialIndex) => {
    const isPortraitMode = OSPicker.getDeviceOrientation() === 'portrait';
    const weekText = (isPortraitMode ? 'Wk ' : 'Week ') + item.week;
    const dayText = isPortraitMode ? ('Day ' + item.day) : ('(Day ' + item.day + ')');
    const textColor = item.active ? 'red' : (itemIndex == (this.state.justUpdated ? this.state.selectedItem : initialIndex) ? '#006400' : 'black');
    const highlightColor = item.week % 2 === 0 ? '#F6F6FE' : 'transparent';
    const backgroundColor = itemIndex == (this.state.justUpdated ? this.state.selectedItem : initialIndex) ? '#F4FEF4' : highlightColor;

    return (
      <MyListItem
           item={item}
           index={itemIndex}
           state={this.props.state}
           style={{height: this.itemHeight - this.separatorHeight}}
           disabled={this.props.state.isCompleted}
           dayText={dayText}
           weekText={weekText}
           textColor={textColor}
           backgroundColor={backgroundColor}
           selectItem={this.selectItem.bind(this)}
           viewItem={this.viewItem.bind(this)}
           updateItem={this.updateItem.bind(this)}
           renderHelpDialog={this.renderHelpDialog.bind(this)}
      />
    );
  }

  renderSeparator = () => {
    return (
      <View
        style={{
          height: this.separatorHeight,
          width: '100%',
          backgroundColor: '#607D8B',
        }}
      />
    );
  }

  renderSwipe(item, itemIndex, parentState) {
    const hiddenWidth = (itemIndex == this.state.selectedItem && item.active && !item.done && !this.props.state.isCompleted) ? 75 : 0;

    return (
      <Text adjustsFontSizeToFit={true} style={{width: hiddenWidth, height: this.itemHeight - 1, alignSelf: 'center', textAlign: 'center', textAlignVertical: 'center', backgroundColor: '#1ef451'}} onPress={() => { this.updateItem(item, parentState); }}>{'Finish'}</Text>
    );
  }

  updateItem(item, parentState) {
    if (item.active && !item.done) {
      OSPicker.showConfirmDialog('Finish Workout', 'Are you sure?', null, true, () => {
        let nextId = parentState.workoutData.findIndex(i => i.id === item.id) + 1;
        const nextItem = parentState.workoutData[nextId];
        nextId = nextItem ? nextId : 0;
        WorkoutSettings.getWorkout(parentState.currentWorkoutId).then((workout) => {
          workout.schedule.filter(i => i.id === item.id)[0].active = false;
          workout.schedule.filter(i => i.id === item.id)[0].done = true;
          this.swipeableFlatList.closeChild();
          if (nextItem) {
            workout.schedule[nextId].active = true;
          }
          WorkoutSettings.updateWorkout(parentState.currentWorkoutId, workout).then((workouts) => {
            this.setState({
              justUpdated: true,
              workoutPage: this.getWorkoutPage(nextId),
              selectedItem: nextId
            }, () => {
              this.props.onupdate(nextId, 'selectedWorkout');
              this.props.onupdate(workouts[parentState.currentWorkoutId].schedule, 'workoutData');
              UserSettings.updateProgress().then(workoutProgress => {
                const tdate = (new Date()).toString();
                const goalUpdateObj = Object.assign({ progress: workoutProgress }, workoutProgress >= 1.0 ? { edate: tdate } : {});
                GoalSettings.updateGoal(parentState.currentWorkoutId, goalUpdateObj)
                .catch(error => {
                  console.error(error.message);
                }).finally(() => {
                  if (workoutProgress >= 1.0) {
                    OSPicker.playStatusComplete(true);
                    this.props.onupdate(true, 'isCompleted');
                  }
                });
              }).then(() => {
                OSPicker.playSelectSound('lock');
              });
            });
          });
        });
      });
    }
  }

  selectItem(item, itemIndex, parentState) {
    this.setState({
      justUpdated: true,
      selectedItem: itemIndex
    }, () => {
      this.swipeableFlatList.closeChild();
    });
  }

  viewItem(item, itemIndex) {
    Alert.alert(
      'Week ' + item.week + ' (Day ' + item.day + ') - Level ' + OSPicker.numberToRomanNumeral(item.level),
      (item.done ? 'Previous' : (item.active ? 'Current' : 'Future')) + ' Workout: \n\n' + item.reps + ' rolls with ' + OSPicker.wordToProperCase(item.dice) + '/Repeater Dice',
      [],
      { cancelable: true }
    );
  }

  getItemLayout = (data, index) => {
    return { length: this.itemHeight, offset: this.itemHeight * index, index };
  }

  onLayout(initialIndex, screenWidth) {
    this.swipeableFlatList.scrollToIndex({ index: initialIndex, animated: true });
  }

  refreshData(data, rfunc, modifier, isWithinRange, ms) {
    if (isWithinRange) {
      setTimeout(() => {
        this.setState({
          justUpdated: false,
          workoutPage: this.state.workoutPage + modifier
        });
        rfunc();
      }, ms);
    }
  }

  render() {
    content = ( <ScrollView contentContainerStyle={styles.container}><Text>{!this.props.state.isInitialized ? 'Loading...' : 'There are no workouts to view.'}</Text></ScrollView> );
    if (this.props.state.workoutData.length) {
      let animatedView;
      const flatListData = this.props.state.workoutData.slice((this.state.workoutPage * this.dataFeedLength), ((this.state.workoutPage + 1) * this.dataFeedLength));
      const dataSelectedIndex = flatListData.findIndex(i => i.active === true);
      const flatListDataSelectedIndex = (dataSelectedIndex > 0) ? dataSelectedIndex : 0;
      if (this.props.state.isCompleted) {
        const screenDiagonal = Math.hypot(this.props.state.screenWidth, this.props.state.screenHeight);
        const radians = Math.atan(this.props.state.screenHeight / this.props.state.screenWidth);
        const degrees = OSPicker.radiansToDegrees(radians);
        const x = (2/3) * this.props.state.screenWidth;
        const y = 80;
        const textWidth = Math.abs(x * Math.sin(radians)) + Math.abs(y * Math.cos(radians));
        const textHeight = Math.abs(x * Math.cos(radians)) + Math.abs(y * Math.sin(radians));
        animatedView = (
          <View pointerEvents='box-none' style={{position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, justifyContent: 'center', alignItems: 'center', zIndex: OSPicker.getMaxInteger() / 2}}>
            <Animatable.Text
              animation={'fadeIn'}
              adjustsFontSizeToFit={true}
              numberOfLines={1}
              iterationCount={'infinite'}
              useNativeDriver={OSPicker.isPlatformIOS()}
              direction={'alternate'}
              style={{color: '#000000', fontSize: screenDiagonal * 0.10, textShadowColor: 'blue', textShadowOffset: { width: 3, height: 3 }, textShadowRadius: 1, transform: [{ rotate: '-'  + degrees + 'deg'}]}}>
                    COMPLETE
            </Animatable.Text>
          </View>
        );
      }
      content = (
        <View style={styles.scontainer}>
          {animatedView}
          <SwipeableFlatList
            ref={(swipeableFlatList) => { this.swipeableFlatList = swipeableFlatList; }}
            data={flatListData}
            containerStyle={{flexShrink: 1}}
            initialNumToRender={this.dataFeedLength}
            overflowHeight={this.separatorHeight}
            showsVerticalScrollIndicator={false}
            ItemSeparatorComponent={this.renderSeparator}
            keyExtractor={item => item.id}
            useRefreshList={true}
            topIndicatorComponent={Indicator}
            bottomIndicatorComponent={Indicator}
            topPullingPrompt={'pull down to load more'}
            topHoldingPrompt={'will load more'}
            topRefreshingPrompt={'loading...'}
            bottomRefreshingPrompt={'loading...'}
            showTopIndicator={this.state.workoutPage > 0}
            showBottomIndicator={((this.state.workoutPage + 1) * this.dataFeedLength) < this.props.state.workoutData.length}
            renderItem={({item, index}) => this.renderWorkouts(item, index, flatListDataSelectedIndex)}
            renderLeft={({item, index}) => this.renderSwipe(item, index, this.props.state)}
            onRefreshing={() => new Promise((r) => {
              this.refreshData(flatListData, r, -1, this.state.workoutPage > 0, 250);
            })}
            onLoadMore={() => new Promise((r) => {
              this.refreshData(flatListData, r, 1, ((this.state.workoutPage + 1) * this.dataFeedLength) < this.props.state.workoutData.length, 250);
            })}
            getItemLayout={this.getItemLayout}
          />
        </View>
      );
    }
    return content;
  }
}

class WorkoutScreen extends React.Component {
  static navigationOptions = {
      title: 'BazzTech™️ - Dice-Fit Trainer'
  };

  constructor(props) {
      super(props);
      this.state = {
        valids: {},
        isDisabled: false,
        isCompleted: false,
        showComponent: true,
        isInitialized: false,
        addWorkoutText: '',
        currentWorkoutId: 0,
        keyboardSpace: 0,
        screenWidth: 0,
        screenHeight: 0,
        footerHeight: 30,
        selectedWorkout: 0,
        deviceOrientation: OSPicker.getDeviceOrientation(),
        optionData: [],
        workoutData: []
      };
      this.toggleRouteButton = this.toggleRouteButton.bind(this);
      this.updateFormFields= this.updateFormFields.bind(this);
      Dimensions.addEventListener('change', () => {
        this.setState({
          screenWidth: Dimensions.get('window').width,
          screenHeight: Dimensions.get('window').height,
          deviceOrientation: OSPicker.getDeviceOrientation()
        });
      });
      Keyboard.addListener('keyboardDidShow', (frames) => {
        if (!frames.endCoordinates) return;
        this.setState({keyboardSpace: frames.endCoordinates.height});
      });
      Keyboard.addListener('keyboardDidHide', (frames) => {
        this.setState({keyboardSpace: 0 });
      });
  }

  componentDidMount() {
    const params = this.props.navigation.state.params;
    GlobalSettings.getSettings(params && params.user && params.user.settings ? params.user.settings.units : null).then(settings => {
      UserSettings.getUser().then((user) => {
        WorkoutSettings.getWorkout(user.workoutId).then((workout) => {
          GoalSettings.getGoal(user.workoutId).then((goal) => {
            this.setState({
              optionData: goal ? goal.options : [],
              workoutData: workout.schedule,
              currentWorkoutId: user.workoutId,
              isCompleted: user.progress >= 1.0,
              isInitialized: true,
              selectedWorkout: workout && workout.schedule ? workout.schedule.findIndex(item => item.active === true) : 0
            });
          });
        });
      });
    });
  }

  componentWillMount() {
    this.setState({
      screenWidth: Dimensions.get('window').width,
      screenHeight: Dimensions.get('window').height,
      deviceOrientation: OSPicker.getDeviceOrientation()
    });
  }

  toggleRouteButton() {
    this.setState(state => {
      disabled: state.isDisabled
    });
  }

  updateFormFields(value, id1, id2 = null) {
    let obj = this.state[id1];
    if (id2 === null) {
      obj = value;
      this.setState({
        [id1]: value
      });
    } else {
      obj[id2] = value;
      this.setState({
        [id1]: obj
      });
    }
  }

  setDisabled(hideView) {
    this.setState({
      isDisabled: true,
      showComponent: hideView ? false : true
    });
  }

  resetDisabled() {
    this.setState({
      isDisabled: false
    });
  }

  render() {
    const { state, navigate } = this.props.navigation;
    const isPortraitMode = this.state.deviceOrientation === 'portrait';
    let noteTitle = isPortraitMode ? 'Notes' : 'View Notes';
    let counterTitle = isPortraitMode ? 'Set Counter' : 'Use Set Counter';
    let goalTitle = isPortraitMode ? 'Goals' : 'Review Goals';
    if ((isPortraitMode || this.state.screenWidth <= 480) && OSPicker.getDeviceAspectRatio() <= 1.5) {
      noteTitle = 'Notes';
      counterTitle = isPortraitMode ? 'Counter' : 'Set Counter';
      goalTitle = 'Goals';
    }
    const popupDialogHeight = isPortraitMode ? 0.225 : 0.375;
    const rbuttons = [
        { title: noteTitle, func: () => { this.setDisabled(true); navigate("LoadScreen", {screen: "NoteScreen", user: state.params.user}); }, disabled: this.state.isDisabled },
        { title: counterTitle, func: () => { this.setDisabled(true); navigate("LoadScreen", {screen: "CounterScreen", user: state.params.user}); }, disabled: this.state.isDisabled },
        { title: goalTitle, func: () => { this.setDisabled(true); navigate("LoadScreen", {screen: "GoalScreen", user: state.params.user}); }, disabled: this.state.isDisabled }
    ];
    if (!this.state.showComponent) {
      return null;
    } else {
      return (
        <View style={{flex: 1, flexDirection: 'column', justifyContent: 'space-between'}}>
          <ViewHeader state={this.state} onchange={this.toggleRouteButton} onupdate={this.updateFormFields} />
          <Portal state={this.state} onchange={this.toggleRouteButton} onupdate={this.updateFormFields} />
          <RouteButtons buttons={rbuttons} />
        </View>
      );
    }
  }
};

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    flexDirection: 'column',
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  scontainer: {
    flex: 1,
    alignSelf: 'stretch',
    backgroundColor: '#F5FCFF',
  }
});

export default WorkoutScreen;
