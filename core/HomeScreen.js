import React, { Component } from 'react';
import { Animated, Dimensions, Easing, ScrollView, StyleSheet, Text, View } from 'react-native';
import TimerEnhance from 'react-native-smart-timer-enhance';
import Toast from 'react-native-smart-toast';

import OSPicker from './utils/OSHelper.js';
import RouteButtons from './utils/RouteHelper.js';
import UserSettings from './utils/UserHelper.js';

import WorkoutSettings from './utils/WorkoutHelper.js';

class Portal extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      buttonShow: true,
      fadeAnim: new Animated.Value(0), // Initial value for opacity: 0
      offsetyAnim: new Animated.Value(100), // Initial value translateY: 100
    };
  }

  componentDidMount() {
    Animated.parallel([
      Animated.timing( // Animate over time
        this.state.fadeAnim, // The animated value to drive
        {
          toValue: 1, // Animate to opacity: 1 (opaque)
          duration: 8000, // Make it take a while
        }
      ),
      Animated.timing(
        this.state.offsetyAnim,
        {
          toValue: 0,
          easing: Easing.back(),
          duration: 4000,
        }
      )
    ]).start(() => this.hideButton()); // Starts the animation
  }

  hideButton() {
    this.setState({buttonShow: false});
  }

  render() {
    const { buttonShow, fadeAnim, offsetyAnim } = this.state;
    if (buttonShow) {
      return (
        <ScrollView contentContainerStyle={styles.container}>
          <Animated.View style={{opacity: fadeAnim, transform: [{translateY: offsetyAnim}]}}>
            <Text style={{fontSize: 25}}>{OSPicker.getGreeting()}</Text>
          </Animated.View>
        </ScrollView>
      );
    } else {
      return (
        <ScrollView contentContainerStyle={styles.container}>
          <Text style={{fontSize: 35}}>Dice-Fit Trainer</Text>
          <Text style={{fontSize: 30, marginBottom: 15}}>BazzTech™️</Text>
        </ScrollView>
      );
    }
    return ( <ScrollView contentContainerStyle={styles.container}><Text>{'Content failed to load'}</Text></ScrollView> );
  }
}

class HomeScreen extends React.Component {
  static navigationOptions = {
    title: 'BazzTech™️ - Dice-Fit Trainer'
  };

  constructor(props) {
      super(props);
      this.state = {
        isProfileRegistered: false,
        showComponent: true,
        user: {},
        screenWidth: 0,
        screenHeight: 0,
        deviceOrientation: OSPicker.getDeviceOrientation(),
      };
      Dimensions.addEventListener('change', () => {
        this.setState({
          screenWidth: Dimensions.get('window').width,
          screenHeight: Dimensions.get('window').height,
          deviceOrientation: OSPicker.getDeviceOrientation()
        });
      });
  }

  componentDidMount() {
    const params = this.props.navigation.state.params;
    UserSettings.getUser((params && params.user) ? params.user : null).then((user) => {
      if (!OSPicker.isUndefinedOrNull(user)) {
        this.setState({
          isProfileRegistered: !OSPicker.isUndefinedOrNull(user.regDate)
        }, () => {
          if (!this.state.isProfileRegistered && (params && (OSPicker.isUndefinedOrNull(params.toast) || (params.toast.indexOf('register') === -1 && params.toast.indexOf('registration') === -1)))) {
            UserSettings.registerUser(user).then((toastMessage) => {
              OSPicker.showHelpDialog('In order to prevent injury and maximize physical fitness, be sure to stretch and drink water before and after workouts.', 'Warning', () => {
                this.showToast(toastMessage);
              }, true);
            });
          }
        });
      }
    })
    .catch(error => {
      console.error(error.message);
    });
  }

  hideView() {
    this.setState({ showComponent: false });
  }

  showToast(msg) {
    if (this.toast) {
      this.toast.show({
        position: Toast.constants.gravity.bottom,
        children: <View><Text style={{color: '#ffffff'}}>{msg}</Text></View>
      });
    }
  }

  onLayout(params) {
    if (params && params.toast) {
      this.showToast(params.toast);
    }
  }

  render() {
    const { state, navigate } = this.props.navigation;
    const isPortraitMode = this.state.deviceOrientation === 'portrait';
    const userData = state.params ? state.params.user : null;
    const registerTitle = isPortraitMode ? 'Register' : 'Registration';
    const settingsTitle = isPortraitMode ? 'Settings' : 'User Settings';
    const statusTitle = isPortraitMode ? 'Status' : 'Check Status';
    let rbuttons = [];
    if (this.state.isProfileRegistered) {
      rbuttons = rbuttons.concat([{ title: settingsTitle, func: () => { this.hideView(); navigate("LoadScreen", {screen: "SettingsScreen", user: userData}); } }]);
      rbuttons = rbuttons.concat([{ title: statusTitle, func: () => { this.hideView(); navigate("LoadScreen", {screen: "StatusScreen", user:  userData}); } }]);
    } else {
      rbuttons = rbuttons.concat([{ title: registerTitle, func: () => { this.hideView(); navigate("LoadScreen", {screen: "RegisterScreen", user: userData}); } }]);
    }
    if (!this.state.showComponent) {
      return null;
    } else {
      return (
        <View style={{flex: 1, flexDirection: 'column', justifyContent: 'space-between'}} onLayout={() => { this.onLayout(state.params); }}>
          <Portal state={this.state} />
          <Toast ref={(toast) => { this.toast = toast; }} style={{marginBottom: 64, backgroundColor: 'rgba(0,128,0,0.4)'}}></Toast>
          <RouteButtons buttons={rbuttons} />
        </View>
      );
    }
  }
};

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    flexDirection: 'column',
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  }
});

export default TimerEnhance(HomeScreen);
