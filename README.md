# Workout Dice Trainer

This cross-platform (iOS/Android) application will assist users with their fitness goals and complement their Workout Dice experience.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

You will need a Windows and MacOSX (or virtual machine) system.

* Node
* Atom IDE
* Android SDK (Windows)

```
Note: For live development, a compatible iOS/Android phone or emulator is required.  Expo, an open source toolchain app, should also be installed on your phone.
```

### Installing

Follow these instructions step-by-step to get the development environment up and running.

Downgrade NPM:

```
npm install -g npm@4
```

Debug via USB (Android):

Note: Beforehand, disable the wireless/ethernet adapters. After the JavaScript bundle builds, enable all wireless/ethernet adapters.

```
adb devices
adb -s <device name> reverse tcp:8081 tcp:8081

npm install
npm run android
```

Debug via WiFi (Android/iOS):

```
npm install
npm start
```
And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [React Native](https://facebook.github.io/react-native/docs/) - The web framework used
* [Android SDK](https://developer.android.com/studio/) - Software Development Kit
* [Adobe Photoshop](https://helpx.adobe.com/photoshop/tutorials.html) - Used to create graphical content

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Nashid Shabazz** - *Initial work* - [DärcKøder](https://github.com/nshid)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone who's code was used
* Inspiration
* etc
